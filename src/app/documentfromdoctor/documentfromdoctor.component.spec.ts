import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentfromdoctorComponent } from './documentfromdoctor.component';

describe('DocumentfromdoctorComponent', () => {
  let component: DocumentfromdoctorComponent;
  let fixture: ComponentFixture<DocumentfromdoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentfromdoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentfromdoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
