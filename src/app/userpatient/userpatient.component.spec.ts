import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserpatientComponent } from './userpatient.component';

describe('UserpatientComponent', () => {
  let component: UserpatientComponent;
  let fixture: ComponentFixture<UserpatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserpatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserpatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
