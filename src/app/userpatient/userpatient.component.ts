import { Component, OnInit } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { PatientService } from '../services/patient.service';
import { PracticeService } from '../services/practice.service';
import { ToastrManager } from "ng6-toastr-notifications";

@Component({
  selector: 'app-userpatient',
  templateUrl: './userpatient.component.html',
  styleUrls: ['./userpatient.component.css']
})
export class UserpatientComponent implements OnInit {
  dataSource: any;
  newpractice: any;
  loader=false;
  allPractices:any
  practice:any;
  searchString:any;
  displayedColumns = ['patientId', 'Name', 'attorney', 'doctor', 'claimNo', 'policyNo', 'doa', 'Tasks'];
  constructor(public toastrManager:ToastrManager,public practiceService: PracticeService, public dialog: MatDialog, public patientService: PatientService,
    private router: Router) {
    if (localStorage.getItem('USER_TOKEN')) {
      this.practiceService.getAllPractice().subscribe(data => {
        this.newpractice = data;
        // this.patientService.getAllPatients().subscribe(patientData=>{
        //   this.dataSource = patientData;
        // })
        console.log(this.newpractice);
      })
    } else {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
      // this.patientService.getAllPatients().subscribe(data => {
      //   this.allPractices = data;
      //   //console.log(this.dataSource);
      // });
  }

  openEditPatientDialog(patientId){
    console.log(patientId);
    this.router.navigate(['patient/edit/'+patientId.id])
  }

  navigateDosPage(patientCode,practiceCode){
    console.log(practiceCode)
    this.router.navigate(['mdmuser/patientdos/'+patientCode+'/'+practiceCode])
  }

  demographicCode(patientCode,practiceCode){
    this.loader=true;
    console.log(patientCode);
    console.log(practiceCode);
    this.patientService.demographicDoc({"practiceCode":practiceCode,"patientCode":patientCode}).subscribe(data=>{
      console.log(data);
      console.log(data.fileContent);
    
    var dataURI = "data:application/pdf;base64," + data.fileContent;
    //window.open(dataURI);  
    //var string = doc.output('datauristring');
    var iframe = "<iframe width='100%' height='100%' src='" + dataURI + "'></iframe>"
    var x = window.open();
    x.document.open();
    x.document.write(iframe);
    x.document.close();
    this.loader=false;
    });
    //console.log(demographoc)
  }
  searchingString(){
    var practiceCode;
    console.log(this.practice);
    console.log(this.newpractice)
    for(var i=0;i<this.newpractice.length;i++){
      //console.log(this.newpractice[i].id)
      if(this.practice == this.newpractice[i].id){
        practiceCode = this.newpractice[i].practiceCode;
      }
    }
    console.log(practiceCode)
    this.patientService.getCodePatients(practiceCode).subscribe(patientData=>{
      this.dataSource = patientData;
    },error=>{
      this.toastrManager.errorToastr("Something went wrong (or) no records found.","Error.")
    })
  }

}
