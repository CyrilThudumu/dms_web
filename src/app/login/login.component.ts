import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: any;
  password: any;
  currentDetails: any;

  constructor(private route: ActivatedRoute,
    private router: Router, public authenticationService: AuthenticationService) { }

  ngOnInit() {
    if (localStorage.getItem('USER_TOKEN')) {  
      if (localStorage.getItem('USER_ROLE') === 'admin') {
          localStorage.setItem("USER_ROLE", 'admin');
          this.router.navigate(['admin']);
      }
  } else {
      console.log('dskhfgsjkdfhjshfd')
      this.username = '';
      this.password = '';
      

      // reset login status
      //this.authenticationService.logout();

      // get return url from route parameters or default to '/'
      //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
    
  }
  onSubmit(username, password) {
    console.log(username);
    console.log(password);
    this.authenticationService.login(username, password).subscribe(data => {
      console.log(data);
      if (data.id_token) {
        localStorage.setItem("USER_TOKEN", data.id_token);
        this.authenticationService.getUserDetails(localStorage.getItem('USER_TOKEN')).subscribe(loggedInUsser => {
          console.log(loggedInUsser);
          this.currentDetails = loggedInUsser;
          localStorage.setItem("USER_NAME", this.currentDetails.firstName);
          localStorage.setItem("USER_ID", this.currentDetails.id);
          // console.log(this.currentDetails.authorities.indexOf("ROLE_ADMIN") > -1);
          // this.waitLogin();
          if (this.currentDetails.authorities.indexOf("ROLE_ADMIN") > -1) {
            localStorage.setItem("USER_ROLE", 'admin');
            this.router.navigate(['admin']);
            return;
          }
          if (this.currentDetails.authorities.indexOf("ROLE_ATTORNEY") > -1) {
            localStorage.setItem("USER_ROLE", 'attorney');
            this.router.navigate(['mdmuser/attorneypatients']);
            return;
          }
          if (this.currentDetails.authorities.indexOf("ROLE_DOCTOR") > -1) {
            localStorage.setItem("USER_ROLE", 'doctor');
            this.router.navigate(['doctor']);
            return;
          }
          if (this.currentDetails.authorities.indexOf("ROLE_USER") > -1) {
            localStorage.setItem("USER_ROLE", 'user');
            this.router.navigate(['mdmuser/patients']);
            return;
          }
        })
      } else {
        this.username = '';
        this.password = '';
      }
    });
  }

}
