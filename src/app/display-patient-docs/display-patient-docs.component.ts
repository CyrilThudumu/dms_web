import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { PatientService } from '../services/patient.service';
import { Router, ActivatedRoute } from '@angular/router';
//import { doc } from '../_models/doc';
import { AssigndocstoattroneyComponent } from '../assigndocstoattroney/assigndocstoattroney.component';
//import { DataService } from '../_services/data.service';
//import { attorneydos } from '../_models/attorneydos';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AttorneypatientsService } from '../services/attorneypatients.service'

@Component({
  selector: 'app-display-patient-docs',
  templateUrl: './display-patient-docs.component.html',
  styleUrls: ['./display-patient-docs.component.css']
})
export class DisplayPatientDocsComponent implements OnInit {

  id: string;
  //patientr: doc[]=[];
  practiceCode: string;
  patientr: any = [];
  doss: any;
  indexx: any;
  document: any;
  dataSource;
  nwdata: any;
  attorneyDos: any = [];
  //attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
  currentPatient: any = [];
  addingDates: any;
  newDataValue: any;
  getDataValue: any = [];
  getpatientDetail: any;
  getPatientRecord: any = [];
  wedUrlPath: any;
  //storagemodel :doc[] =JSON.parse(localStorage.getItem('model')) || [];
  displayedColumns = ['patientId', 'Name', 'Age', 'gender', 'phone', 'Practice', 'doa', 'doctor'];


  constructor(
    public attorneypatientsService:AttorneypatientsService,
    private patientService: PatientService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public toastrManager: ToastrManager
    //private dataService:DataService,

  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.practiceCode = this.route.snapshot.paramMap.get('practice');
    console.log('id is ', this.id);
    this.patientService.getAllPatients().subscribe(data => {
      this.getpatientDetail = data;
      //console.log(this.getpatientDetail)
      // this.currentPatient = this.getpatientDetail.filter(patientData=>{
      //   console.log(patientData.patientCode == this.id);
      //   console.log('#$@#$@#$@#$@#$#@')
      //   return patientData.patientCode == this.id
      // })
      // this.currentPatient = this.getPatientRecord[0];
      // console.log(this.currentPatient)

      for (var i = 0; i < this.getpatientDetail.length; i++) {
        if (this.getpatientDetail[i].patientCode == this.id) {
          this.currentPatient[0] = this.getpatientDetail[i];
        }
      }
      this.dataSource = this.currentPatient;
      console.log(this.dataSource)
    })
    this.patientService.getPatientDOS(this.id, this.practiceCode).subscribe(data => {
      console.log(data);
      this.newDataValue = data;
      this.addingDates = this.newDataValue.content.map(dateValue => {
        dateValue.appendDate = dateValue.fromDos + ' To ' + dateValue.toDos;
        return dateValue;
      })


      var hash = Object.create(null),
        result = [];

      this.addingDates.forEach(function (a) {
        if (!hash[a['appendDate']]) {
          hash[a['appendDate']] = [];
          result.push(hash[a['appendDate']]);
        }
        hash[a['appendDate']].push(a);
      });
      console.log("@@@@@@@@@@@@@@@@", result.length)
      //this.patientr.newDocType = []
      var dateGet = '';
      var doctypeValue = [];
      var doctypeLink = [];
      for (var i = 0; i < result.length; i++) {
        console.log(result[i])
        for (var j = 0; j < result[i].length; j++) {
          console.log(result[i][j])
          //this.patientr.push({"dosDate":result[i][0].appendDate});
          dateGet = result[i][j].appendDate;
          var splitString = result[i][j].documentName.split('.');
          var splittedStirng = splitString[0];
          var last10Char = splittedStirng.slice(-10);
          console.log(last10Char)
          doctypeValue.push(result[i][j].documentTypes+'_'+last10Char);
          doctypeLink.push(result[i][j])

        }
        this.patientr.push({ "dosDate": dateGet, "docType": doctypeValue, "docLink": doctypeLink, "attorneyName": doctypeLink[0].attorneyCode, "processedValue": doctypeLink[0].processedFlag, "fromDos": doctypeLink[0].fromDos, "toDOS": doctypeLink[0].toDos,"processedDate":doctypeLink[0].processedDate });
        console.log(doctypeLink[0].attorneyCode)
        dateGet = '';
        doctypeValue = [];
        doctypeLink = [];

      }

      console.log(this.patientr)

      // foreach(var groupName in groups) {
      //   this.getDataValue.push({ group: groupName, color: groups[groupName] });
      // }
      // for(var groupName in groups){
      //   console
      // }

      //for(var i=0;i<groups.length;i++){

      //}

      console.log(JSON.stringify(this.addingDates));
      console.log('$$$$$$$$$$$$$$')
    }, error => {
      this.toastrManager.errorToastr("Something went wrong. Reload the page.", "Error")
    })
    // if (this.id) {
    //   // let patient;
    //   //this.patientr = this.patientService.getPatientByIdfrdis(this.id); 
    //   console.log('patienr frm service ', this.patientr)
    //   // this.patientr=patient;
    //   // this.patientr = this.patientService.getattorneydos(this.id);
    //   //this.currentPatient[0] =this.patientService.get(this.id);
    // }
    // window.location.reload();

  }
  reinitializeData() {
    this.patientr = [];
    this.patientService.getPatientDOS(this.id, this.practiceCode).subscribe(data => {
      console.log(data);
      this.newDataValue = data;
      this.addingDates = this.newDataValue.content.map(dateValue => {
        dateValue.appendDate = dateValue.fromDos + ' To ' + dateValue.toDos;
        return dateValue;
      })


      var hash = Object.create(null),
        result = [];

      this.addingDates.forEach(function (a) {
        if (!hash[a['appendDate']]) {
          hash[a['appendDate']] = [];
          result.push(hash[a['appendDate']]);
        }
        hash[a['appendDate']].push(a);
      });
      console.log("@@@@@@@@@@@@@@@@", result.length)
      //this.patientr.newDocType = []
      var dateGet = '';
      var doctypeValue = [];
      var doctypeLink = [];
      for (var i = 0; i < result.length; i++) {
        console.log(result[i])
        for (var j = 0; j < result[i].length; j++) {
          console.log(result[i][j])
          //this.patientr.push({"dosDate":result[i][0].appendDate});
          dateGet = result[i][j].appendDate;
          var splitString = result[i][j].documentName.split('.');
          var splittedStirng = splitString[0];
          var last10Char = splittedStirng.slice(-10);
          console.log(last10Char)
          doctypeValue.push(result[i][j].documentTypes+'_'+last10Char);
          doctypeLink.push(result[i][j])

        }
        this.patientr.push({ "dosDate": dateGet, "docType": doctypeValue, "docLink": doctypeLink, "attorneyName": doctypeLink[0].attorneyCode, "processedValue": doctypeLink[0].processedFlag, "fromDos": doctypeLink[0].fromDos, "toDOS": doctypeLink[0].toDos,"processedDate":doctypeLink[0].processedDate });
        console.log(doctypeLink[0].attorneyCode)
        dateGet = '';
        doctypeValue = [];
        doctypeLink = [];

      }

      console.log(this.patientr)

      // foreach(var groupName in groups) {
      //   this.getDataValue.push({ group: groupName, color: groups[groupName] });
      // }
      // for(var groupName in groups){
      //   console
      // }

      //for(var i=0;i<groups.length;i++){

      //}

      console.log(JSON.stringify(this.addingDates));
      console.log('$$$$$$$$$$$$$$')
    }, error => {
      this.toastrManager.errorToastr("Something went wrong. Reload the page.", "Error")
    })
  }

  downloadDocTypes(patientDetails){
    // console.log(patientDetails);
    // //{patientCode}/{practiceCode}/{fromDos}/{toDos}
    // this.attorneypatientsService.getDownloads({"patientCode":patientDetails.docLink[0].patientCode,"practiceCode":patientDetails.docLink[0].practiceCode,"fromDos":patientDetails.docLink[0].fromDos,"toDos":patientDetails.docLink[0].toDos}).subscribe(data=>{
    //   console.log("SADFASDFASDFASDFASDFASDF");
    //   console.log(data)
    // },error=>{
    //   console.log("SDFsADFsadfasDFasd");
    //   console.log(error)
    // })

    for(var i=0;i<patientDetails.docLink.length;i++){
      var sampleArr = this.base64ToArrayBuffer(patientDetails.docLink[i].fileContent);
      var splitString = patientDetails.docLink[i].documentName.split('.');
      var splittedStirng = splitString[0];
      var last10Char = splittedStirng.slice(-10);
      console.log(patientDetails)
      
      this.saveByteArray(patientDetails.docLink[i].practiceCode+'_'+patientDetails.docLink[i].patientCode+'_'+this.dataSource[0].firstName+'-'+this.dataSource[0].lastName+'_'+patientDetails.docLink[i].fromDos+'_'+patientDetails.docLink[i].toDos+'_'+patientDetails.docLink[i].documentTypes+'_'+last10Char, sampleArr);
    }
  }

    saveByteArray(reportName, byte) {
    var blob = new Blob([byte], {type: "application/pdf"});
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName;
    link.download = fileName;
    link.click();
  }

  base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
       var ascii = binaryString.charCodeAt(i);
       bytes[i] = ascii;
    }
    return bytes;
 }


  openInNextTab(tabLink, index) {
    console.log(tabLink[index]);
    console.log(index);
    var dataURI = "data:application/pdf;base64," + tabLink[index].fileContent;
    //window.open(dataURI);  
    //var string = doc.output('datauristring');
    var iframe = "<iframe width='100%' height='100%' src='" + dataURI + "'></iframe>"
    var x = window.open();
    x.document.open();
    x.document.write(iframe);
    x.document.close();
    // let file = new Blob([tabLink[index].fileContent], { type: 'application/pdf' });            
    // var fileURL = "data:application/pdf;base64,"+tabLink[index].fileContent;//URL.createObjectURL(file);
    // this.wedUrlPath = fileURL;
    //window.open(fileURL);
    //this.router.navigate([]).then(result => {  window.open(tabLink, '_blank'); });
  }
  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("wait for the option to select"));
  }
  ngOnInit() {
    // console.log('in display', this.document);
    //  this.patientService.openAssignAttorneyDialog(dos: any, index: any, patient: any, event: any);
    this.dataSource = new MatTableDataSource(this.currentPatient);

    if (localStorage.getItem('reloadCount') === '0') {
      window.location.reload();
      localStorage.setItem("reloadCount", '1');
    }
  }

  Update() {
    localStorage.setItem("reloadCountDisplayDos", '0');
    let newatorneydos;
    if (this.nwdata) {
      console.log('update ', this.nwdata);
      newatorneydos = this.nwdata;
    }
    else {
      console.log('update is null so attorneydos=patient ', this.patientr);
      console.log('index is ', this.indexx);
      newatorneydos = this.patientr;

    }
    this.attorneyDos.push(newatorneydos);
    localStorage.setItem('attorneydos', JSON.stringify(this.attorneyDos));
    //this.dataService.setData(this.nwdata);
    this.router.navigate(['/mdmuser/patients']);
  }
  open(event: any) {
    event.preventDefault();
  }
  openAssignAttorneyDialog(dos, index, patient, event: any) {
    event.preventDefault();
    console.log(dos)
    console.log(dos)
    const dialogConfig = new MatDialogConfig();
    this.doss = dos;
    this.indexx = index;
    dialogConfig.panelClass = "assignAttorney";
    dialogConfig.id = "practiceForm";
    dialogConfig.data = { dos: this.doss, index: this.indexx, doc: patient, patientData: this.currentPatient[0] };
    const dialogRef = this.dialog.open(AssigndocstoattroneyComponent, dialogConfig);
    dialogRef.componentInstance.onAdd.subscribe((data) => {
      console.log("skdfgskajfh,msafgksafmsagf,hsfd")
      this.nwdata = data;
      //location.reload();
    });
    dialogRef.afterClosed().subscribe(response => this.reinitializeData());
    //this.router.navigate(['mdmuser/patientdos/'+this.id+'/'+this.practiceCode])


  }


}










/*import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { doc } from '../_models/doc';
import { AssigndostoattorneyComponent } from '../assigndostoattorney/assigndostoattorney.component';
import { DataService } from '../_services/data.service';
import { attorneydos } from '../_models/attorneydos';
@Component({
  selector: 'app-display-patient-docs',
  templateUrl: './display-patient-docs.component.html',
  styleUrls: ['./display-patient-docs.component.css']
})
export class DisplayPatientDocsComponent implements OnInit {
  id: string;
  //patientr: doc[]=[];
  patientr;
  doss: any;
  indexx: any;
  document: any;
  dataSource;
  nwdata: any;
   attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
   currentPatient=[];
   //storagemodel :doc[] =JSON.parse(localStorage.getItem('model')) || [];
   displayedColumns = ['patientId','Name', 'Age', 'gender', 'phone', 'Practice', 'doa', 'doctor'];
 
  constructor(
    private patientService : PatientService,
    private router: Router, 
    private route: ActivatedRoute,
    public dialog:MatDialog ,
    private dataService:DataService,
    
  ) { 
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('id is ',this.id)
 
    if (this.id) {
     // let patient;
    this.patientr = this.patientService.getPatientByIdfrdis(this.id); 
    // this.patientr=patient;
     // this.patientr = this.patientService.getattorneydos(this.id);
      this.currentPatient[0] =this.patientService.get(this.id);
        }
       // window.location.reload();
     
  }
  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("wait for the option to select"));
  }
  ngOnInit() {
   // console.log('in display', this.document);
 //  this.patientService.openAssignAttorneyDialog(dos: any, index: any, patient: any, event: any);
 this.dataSource = new MatTableDataSource(this.currentPatient);
 
 if(localStorage.getItem('reloadCount')==='0'){
   window.location.reload();
   localStorage.setItem("reloadCount",'1');
 }
  }
  
  Update(){
    localStorage.setItem("reloadCountDisplayDos", '0');
let newatorneydos;
if(this.nwdata){
  console.log('update ', this.nwdata);
newatorneydos=this.nwdata;
}
else{
  console.log('update is null so attorneydos=patient ', this.patientr);
  console.log('index is ', this.indexx);
  newatorneydos=this.patientr[0];

}
this.attorneyDos.push(newatorneydos);
    localStorage.setItem('attorneydos', JSON.stringify(this.attorneyDos));
    this.dataService.setData(this.nwdata);
    this.router.navigate(['/mdmuser/patients']);
   }
  openAssignAttorneyDialog(dos, index, patient, event:any){
    event.preventDefault();
    const dialogConfig = new MatDialogConfig();
   this.doss=dos;
   this.indexx=index;
   dialogConfig.panelClass="assignAttorney";
   dialogConfig.id="practiceForm";
   dialogConfig.data={dos:this.doss, index:this.indexx, doc:patient,  pdfSrc:this.document};
   const dialogRef = this.dialog.open(AssigndostoattorneyComponent, dialogConfig);
    dialogRef.componentInstance.onAdd.subscribe((data) => {
    this.nwdata=data;
  });
 
   }

   
}
*/