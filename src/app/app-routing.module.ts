import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { UserpatientComponent } from './userpatient/userpatient.component';
import { AddUserPatientComponent } from './add-user-patient/add-user-patient.component';
import { MapdocsComponent } from './mapdocs/mapdocs.component';
import { DoctorComponent } from './doctor/doctor.component';
import { AttorneyComponent } from './attorney/attorney.component';
import { EdituserpatientComponent } from './edituserpatient/edituserpatient.component';
import { MapDocumnetsComponent } from './map-documnets/map-documnets.component';
import { DisplayPatientDocsComponent } from './display-patient-docs/display-patient-docs.component';
import { AttorneypatientsComponent } from './attorneypatients/attorneypatients.component';
import { DocumentfromattorneyComponent } from './documentfromattorney/documentfromattorney.component';
import { EditUserPatientComponent } from './edit-user-patient/edit-user-patient.component';
import { DoctorpatientsComponent } from './doctorpatients/doctorpatients.component';
import { DocumentfromdoctorComponent } from './documentfromdoctor/documentfromdoctor.component';
import { SearchattorneypatientComponent } from './searchattorneypatient/searchattorneypatient.component';
import { AttorneyHistoryComponent } from './attorney-history/attorney-history.component';
import { AttorneyDosHistoryComponent } from './attorney-dos-history/attorney-dos-history.component';
import { ReportsComponent } from './reports/reports.component'

const appRoutes: Routes = [
//{ path: '', component: HomeComponent},
  { path: '', component: LoginComponent },
  // { path: 'register',component: RegisterComponent},
   { path: 'admin', component: AdminComponent},
   { path: 'admin/users', component: AdminUserComponent },
   { path: 'mdmuser', component: AddUserPatientComponent },
   { path: 'mdmuser/patients', component: UserpatientComponent },
   { path: 'mdmuser/mapDocuments', component: MapDocumnetsComponent},
   { path: 'mdmuser/patientdos/:id/:practice', component: DisplayPatientDocsComponent },
   { path: 'mdmuser/docsfromattorney/:id/:practice', component: DocumentfromattorneyComponent },
   { path: 'mdmuser/attorneyhistory/:id/:practice', component: AttorneyDosHistoryComponent },
   { path: 'mdmuser/docsfromdoctor/:id/:practice', component: DocumentfromdoctorComponent },
   { path: 'attorney/history',component:AttorneyHistoryComponent},
   { path: 'reports',component:ReportsComponent},
   //{ path: 'patient/edit', component: EdituserpatientComponent },
   { path : 'patient/edit/:id', component:EditUserPatientComponent},
   { path: 'doctor', component: DoctorpatientsComponent },
   { path: 'attorney', component: AttorneyComponent },
   { path: 'mdmuser/mapDos', component: MapdocsComponent },
   { path: 'mdmuser/attorneypatients', component: AttorneypatientsComponent },
   { path: 'admin/searchattorneypatient', component: SearchattorneypatientComponent }

  // // otherwise redirect to home
  // { path: '**', redirectTo: '' }
];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
export const routing = RouterModule.forRoot(appRoutes);
