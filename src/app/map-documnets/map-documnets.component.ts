import { MapDocumentsService } from '../services/map-documents.service';
import { Component, OnInit, ViewChild, ElementRef, HostListener, Inject, ɵConsole } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { PDFDocumentProxy } from 'pdfjs-dist';
import { MatOption, MatDialog, MatSelect } from '@angular/material';
import { MatSort, MatPaginator, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { DOCUMENT, formatDate } from '@angular/common';
//import { EditPracticeComponent } from '../edit-practice/edit-practice.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { AuthenticationService } from '../services/authentication.service';
import { PracticeService } from '../services/practice.service';
//import { doc } from '../_models/doc';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';
import { ArrayDataSource, DataSource } from '@angular/cdk/collections';
import { isEmpty, map } from 'rxjs/operators';
//import { AlertService, AuthenticationService } from '../_services';
import { PatientService } from '../services/patient.service';
//import { Practice } from '../_models/practice';
import { forEach } from '@angular/router/src/utils/collection';
import { stringify } from '@angular/core/src/util';
//import { User } from '../_models';
//import { PracticeService } from '../practice.service';
import { Router } from '@angular/router';
//import { DataService } from '../_services/data.service';
import { ValueTransformer } from '@angular/compiler/src/util';
import { empty } from 'rxjs';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-map-documnets',
  templateUrl: './map-documnets.component.html',
  styleUrls: ['./map-documnets.component.css']
})
export class MapDocumnetsComponent implements OnInit {
  @ViewChild('stickyMenu') menuElement: ElementRef;
  @ViewChild('mySelect') mySelect;
  @ViewChild('allSelected') private allSelected: MatOption;
  @ViewChild('ev') private ev: MatOption;
  @ViewChild("matdivider") matdivider: ElementRef;
  @ViewChild('searchselect') searchselect;
  documentTypes: string;
  index: number;
  practice: string;
  dosFrom: Date;
  dosTo: Date;
  searchBy: string;
  patients = [];
  patient;
  totalPDFPages: any;
  mapDocFileSystem: any;
  //storagemodel :doc[] =JSON.parse(localStorage.getItem('model')) || [];
  storagemodel: any = JSON.parse(localStorage.getItem('model')) || [];
  searchValue: string;
  patientItem: string;
  disabled: boolean = false;
  updatedDoa: any;
  updatedDob: any;
  addAndUpdate: boolean = true;
  pageNumber: any = [];
  form: FormGroup;
  formD: FormData;
  pageId;
  fileToUpload: File = null;
  pdfSrc;
  isActive: boolean;
  opened: boolean;
  fileSrc;
  page = 1;
  pdf: any;
  isLoaded = false;
  outline: any[];
  totalPages: number;
  remainingPdfPages: any = [];
  selectPageNo;
  filename;
  sticky: boolean = false;
  stickyButton: boolean = false;
  elementPosition: any;
  //patientArray:doc[]=[];
  patientArray: any = [];
  mapAddDoc: any[] = [];
  temp: any[] = [];
  dataSource = null;
  addDisabled: boolean = true;
  displayedColumns = ['Document Type', 'Practice Id', 'doctor', 'Patient Id', 'DOS From - DOS To', 'Pages', 'Actions'];
  newdosFrom: any;
  newdosTo: any;
  mapDetails: any;
  doct: any[] = [];
  totalPageCounter:any;
  rowedData: any = [];
  // newpractice: Practice[];
  newpractice: any;
  checkIfOthersAreSelected;
  remainingPdfPagess;
  doctor: any;
  deletedisabled: boolean = false;
  //user:User[]=[];
  user: any = [];
  pastindex: any;
  editdisabled: boolean = false;
  selectedpatients = [];
  aboutToSave:boolean=true;
  openedfilter: boolean;
  formdoc: any;
  model: any = {};
  filterName: any;
  getDataMapDoc: any;
  dummyMapDoc: any = [];
  currentPage:boolean;

  //mapDocArray:doc[]=[];
  mapDocArray: any = [];
  val;
  pastpatient: boolean;
  existingPatient: boolean;
  constructor(
    private fb: FormBuilder,
    @Inject(DOCUMENT) private document: any,
    public dialog: MatDialog,
    private mapDocService: MapDocumentsService,
    private http: HttpClient,
    // private model:doc,
    // private alertService: AlertService,
    private patientService: PatientService,
    private practiceService: PracticeService,
    private router: Router,
    public authenticationService: AuthenticationService,
    public toastrManager: ToastrManager
  ) {

  }

  ngOnInit() {
    //  this.patients=JSON.parse(localStorage.patients)
    //  console.log(this.patients);
    this.model.practice = [];
    this.model.documentTypes = [];
    this.model.dosFrom = [];
    this.model.dosTo = [];
    this.model.pageNumber = [];
    //this.patients = JSON.parse(localStorage.patients);
    //this.newpractice= JSON.parse(localStorage.practices);
    this.patientService.getAllPatients().subscribe(dataValue => {
      this.patients = dataValue;
      console.log(this.patients);
    })

    this.practiceService.getAllPractice().subscribe(dataPractice => {
      this.newpractice = dataPractice;
    })
    //this.user=this.patientService.getUser('doctor');
    this.authenticationService.getAllUsers().subscribe(data => {
      console.log(data);
      this.user = data.filter(userValue => {
        console.log(userValue.authorities.indexOf("ROLE_DOCTOR"));
        return userValue.authorities.indexOf("ROLE_DOCTOR") > -1;
      });
      console.log(this.user);
    })
    console.log('doctors are ', this.user);
    this.form = this.fb.group({
      documentTypes: ['', Validators.required],
      practice: ['', Validators.required],
      dosFrom: ['', Validators.required],
      dosTo: ['', Validators.required],
      searchBy: ['', Validators.required],
      searchValue: ['', Validators.required],
      patientItem: ['', Validators.required],
      //  dob: ['', Validators.required],
      //  doa: ['', Validators.required],
      pageNumber: ['', Validators.required],
      doctor: ['', Validators.required],
    });
  }



  @HostListener('window:scroll', ['$event'])
  handleScroll() {
    const windowScroll = window.pageYOffset;
    this.elementPosition = this.matdivider.nativeElement.offsetTop;
    if (windowScroll >= this.elementPosition) {
      this.sticky = true;
      this.stickyButton = true;
    } else {
      this.sticky = false;
      this.stickyButton = false;
    }
  }

  handleFileInput(files: FileList) {
    console.log(' trget ', files[0].name)
    const formData = new FormData();
    this.filename = '/mapDocuments/' + files[0].name;
    formData.append('Document', files[0], files[0].name);
    this.formD = formData;
    this.formdoc = this.formD;
    console.log(this.formD.get('Document'));
    this.onFileSelected();
  }

  reinitializeData(mappedData) {
    console.log('reinitialize data', mappedData);
    // this.dataSource = new MatTableDataSource<doc>(mappedData);
    this.dataSource = new MatTableDataSource<any>(mappedData);


  }

  dosFormatDate(dosFrom: Date, dosTo: Date) {
    this.newdosFrom = formatDate(dosFrom, 'MMddyyyy', 'en-us');
    this.newdosTo = formatDate(dosTo, 'MMddyyyy', 'en-us');
  }


  onFileSelected() {
    const $pdf: any = document.querySelector('#file');
    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
      };
      console.log('pdfsrc ', this.pdfSrc)
      reader.readAsArrayBuffer($pdf.files[0]);
    }
  }


  applyFilter(filtervalue: String) {
    //console.log('in apply filter');
    console.log(filtervalue)
    this.patient = this.Filteredpatients(filtervalue);
    if (filtervalue !== "") {
      this.searchselect.open();
    }
    else { this.searchselect.close(); }
    //this.patient.filter=filtervalue.trim();

    //console.log('filtered patient is ',this.patient);
  }

  priorityValue(selectValue) {
    console.log(selectValue);
    this.filterName = selectValue;
  }

  practiceSelection(practiceSelect) {

  }
  Filteredpatients(filterValue) {
    console.log(filterValue);
    if (this.filterName === 'patientId') {
      this.selectedpatients = this.patients.filter((patientID) => {

        return patientID.patientCode.toString().indexOf(filterValue) > -1;

      });

      console.log(this.selectedpatients);
      return this.selectedpatients;
    }
    // if(this.filterName=='patientId')
    // return this.selectedpatients.filter(item=> item.patientId.indexOf(this.filterName)!==-1);
    // console.log(this.selectedpatients);

    if (this.filterName === 'firstName') {
      this.selectedpatients = this.patients.filter((patientFirstName) => {
        return patientFirstName.firstName.toString().indexOf(filterValue) > -1;
      });
      console.log(this.selectedpatients);
      return this.selectedpatients;
    }
    if (this.filterName === 'lastName') {
      this.selectedpatients = this.patients.filter((patientLastName) => {
        return patientLastName.lastName.toString().indexOf(filterValue) > -1;
      });
      console.log(this.selectedpatients);
      return this.selectedpatients;
    }
    //   let selectedPractice=this.form.value.practice;
    //   console.log('in filter selected practice is ', this.patient.filter)
    //  this.selectedpatients= this.patientService.getPatientsByPractice(selectedPractice.practiceName);
    //    if(this.searchBy=='patientId')
    //    return this.selectedpatients.filter(item=> item.patientId.indexOf(this.searchValue)!==-1);
    //    else if(this.searchBy=='firstName')
    //    return this.selectedpatients.filter(item=> item.firstName.indexOf(this.searchValue)!==-1);
    //    else
    //    return this.selectedpatients.filter(item=> item.lastName.indexOf(this.searchValue)!==-1);
  }
  openedChangefilter(openedfilter: boolean) {
    if (openedfilter === true && this.patient) {
      console.log('in opened filter ', openedfilter);
      this.openedfilter = openedfilter;
    }
    return true;
  }
  getPatientById(patientId): any {

    for (let i = 0; i < this.mapDocArray.length; i++) {
      let patitem = this.mapDocArray[i];

      if (patientId === patitem.patientItem) {

        return this.mapDocArray[i];
      }

    }
  }
  selectedPatientValue(patientDetails) {
    // console.log(patientDetails);

    // console.log(this.mapAddDoc);
    if (this.mapAddDoc.length > 0) {
      for (var i = 0; i < this.rowedData.length; i++) {
        if (this.mapAddDoc[i].patientCode == patientDetails.patientCode) {

          for (var j = 0; j < this.rowedData[i].pageNumber.length; j++) {
            this.remainingPdfPages[parseInt(this.rowedData[i].pageNumber[j]) - 1] = null;
          }
          console.log(this.rowedData[i].pageNumber);
          console.log(this.remainingPdfPages);
        }
      }
    }
    this.currentPage = this.remainingPdfPages[this.page-1]
  }
  add() {
    console.log(this.form.value)
    console.log(this.pageNumber);
    
    if (this.pageNumber.length > 0) {
      let formvalue = this.form.value;
      let page_number = ((this.pageNumber).join(','));
      let pageTitle;
      if(this.pageNumber.length>4){
        pageTitle = "More than 4 records"
      }else{
        pageTitle = ((this.pageNumber).join(','));
      }
      
      this.rowedData.push({ "documentTypes": formvalue.documentTypes, "dosFrom": formvalue.dosFrom, "dosTo": formvalue.dosTo,"pageTitle":pageTitle, "pageNumber": this.pageNumber, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": formvalue.searchBy, "searchValue": formvalue.searchValue, "patientItem": formvalue.patientItem });
      this.mapAddDoc.push({ "documentTypes": formvalue.documentTypes, "dosFrom": formvalue.dosFrom, "dosTo": formvalue.dosTo,"pageTitle":pageTitle, "pageNumber": page_number, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": formvalue.searchBy, "searchValue": formvalue.searchValue, "patientItem": formvalue.patientItem, "patientCode": formvalue.patientItem.patientCode });
      console.log(this.mapAddDoc);
      this.reinitializeData(this.mapAddDoc);
      this.form.get('documentTypes').reset();
      this.form.get('patientItem').reset();
      this.form.get('dosFrom').reset();
      this.form.get('dosTo').reset();
      this.pageNumber = [];
      this.remainingPdfPages = [];
      // this.remainingPdfPages = new Array(pdf.numPages);
      // console.log(pdf.numPages);
      for (var i = 0; i < this.totalPDFPages; i++) {
        this.remainingPdfPages.push(true);
      }

    } else {
      //console.log("this is not working");
      this.toastrManager.errorToastr("Pages are not added", "No Pages");

    }
    //if (this.form.invalid) {
    //   return;
    // }
    // this.form.get('patientItem').valueChanges.subscribe(value => {
    //   this.val = true;

    //   console.log(value.patientCode)
    //   // if(this.mapDocArray.length==0)
    //   if (this.val == true
    //     && this.getPatientById(value.patientCode) == undefined
    //   ) {
    //     this.remainingPdfPages = new Array(this.totalPages);
    //     // else{

    //     //   let res;
    //     //  res=this.getPatientById(value.patientCode);
    //     //   this.model=res;
    //     //  this.pastpatient=true
    //     this.existingPatient = false;
    //   }


    //   //  }
    //   console.log('end of value change')
    //   console.log(this.getPatientById(value.patientCode))
    // }
    // )
    // if (this.val == true
    //   //  && this.pastpatient==false
    // ) {
    //   this.mapDocArray.push(JSON.parse(JSON.stringify(this.model)));
    //   console.log('mapdocarray is ', this.mapDocArray);
    //   this.val = false;
    //   this.model.documentTypes = [];
    //   this.model.dosFrom = [];
    //   this.model.dosTo = [];
    //   this.model.pageNumber = [];
    //   this.model.practice = null;
    //   this.model.searchBy = '';
    //   this.model.patientItem = ''
    //   this.model.searchValue = '';
    //   this.model.doctor = null;


    // }
    // this.deletedisabled = false;
    // let formvalue = this.form.value;
    // //var practice='';
    // console.log(formvalue.practice.practiceName);

    // this.model.practice = formvalue.practice.practiceName;
    // //this.model.documentTypes=[];
    // //documentTypes=[];
    // this.model.documentTypes.push(formvalue.documentTypes);
    // this.dosFormatDate(formvalue.dosFrom, formvalue.dosTo);
    // //this.model.dosFrom=[];
    // this.model.dosFrom.push(this.newdosFrom);
    // //this.model.dosTo=[];
    // this.model.dosTo.push(this.newdosTo);
    // this.model.searchBy = formvalue.searchBy;
    // this.model.searchValue = formvalue.searchValue;
    // //  this.model.patientItem = formvalue.patientItem.patientId;
    // this.model.patientItem = formvalue.patientItem.patientCode;
    // this.model.doctor = formvalue.doctor.firstName;
    // console.log(formvalue.pageNumber);

    // let page_number = ((formvalue.pageNumber).join(','));
    // //this.model.pageNumber=[];
    // this.model.pageNumber.push(page_number);
    // console.log('adding data by click add');
    // console.log('model ' + JSON.stringify(this.model));
    // //  this.mapAddDoc.push({ "documentTypes": formvalue.documentTypes, "dosFrom": formvalue.dosFrom, "dosTo": formvalue.dosTo, "pageNumber": page_number, "practice": formvalue.practice.practiceName, "doctor": formvalue.doctor.firstName, "searchBy": formvalue.searchBy, "searchValue": formvalue.searchValue, "patientItem": formvalue.patientItem });
    // this.mapAddDoc.push({ "documentTypes": formvalue.documentTypes, "dosFrom": formvalue.dosFrom, "dosTo": formvalue.dosTo, "pageNumber": page_number, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": formvalue.searchBy, "searchValue": formvalue.searchValue, "patientItem": formvalue.patientItem });
    // this.reinitializeData(this.mapAddDoc);
    // for (let i = 0; i < this.pageNumber.length; i++) {
    //   let j = this.pageNumber[i];
    //   this.remainingPdfPages[j - 1] = null;
    // }
    // this.disabled = true;
    // this.delay(500).then(any => {
    //   this.form.get('documentTypes').reset();
    //   //  this.form.get('dosFrom').reset();
    //   // this.form.get('dosTo').reset();
    //   this.form.get('pageNumber').reset();

    // });
    // console.log('resetted doctypes, dos, pagenumber');
    // console.log('val at last', this.val);
    // console.log(this.mapAddDoc)


  }

  save() {
    console.log('in save');
    this.aboutToSave = false;
    this.deletedisabled = false;
    //this.mapDocArray.push(this.model);
    // let formvalue = JSON.stringify(this.mapDocArray);
    // this.formD.append('FormValue', formvalue);
    // this.formD.append('File', this.formD.get('Document'));
    console.log(this.mapAddDoc);
    var dummyDocs = [];
    dummyDocs = this.mapAddDoc.filter((thing, index, self) =>
      index === self.findIndex((t) => (
        t.patientCode === thing.patientCode
      ))
    )
    console.log(dummyDocs);
    for (var i = 0; i < dummyDocs.length; i++) {
      this.mapDocFileSystem = [];
      if (i == 0) {
        this.mapDocFileSystem = this.mapAddDoc.filter(dataValue => {
          return dataValue.patientCode === dummyDocs[i].patientCode;
        });
        this.dummyMapDoc.push(this.mapDocFileSystem);
      } else {
        for (var j = 0; j < this.dummyMapDoc.length; j++) {
          if (this.dummyMapDoc[j][0].patientCode != dummyDocs[i].patientCode) {
            this.mapDocFileSystem = this.mapAddDoc.filter(dataValue => {
              return dataValue.patientCode === dummyDocs[i].patientCode;
            });
            this.dummyMapDoc.push(this.mapDocFileSystem);
          }
        }

      }

    }
    console.log(this.dummyMapDoc);
    //return;
    this.getDataMapDoc = [];
    var pageTotalCount=0;
    for (var i = 0; i < this.dummyMapDoc.length; i++) {
      var documentTypes = [];
      var dosFrom = [];
      var dosTo = [];
      var pageNumber = [];
      var patientCode = '';
      var practice = '';
      var searchBy = '';
      var searchValue = '';
      var doctor = '';

      // this.getDataMapDoc[i].documentTypes=[];
      // this.getDataMapDoc[i].dosFrom=[];
      // this.getDataMapDoc[i].dosTo=[];
      // this.getDataMapDoc[i].pageNumber=[];
      for (var j = 0; j < this.dummyMapDoc[i].length; j++) {
        var pagecountArray = this.dummyMapDoc[i][j].pageNumber.split(",");
        pageTotalCount = pageTotalCount+pagecountArray.length;
        patientCode = this.dummyMapDoc[i][0].patientCode;
        practice = this.dummyMapDoc[i][0].practice.practiceCode;
        searchBy = this.dummyMapDoc[i][0].searchBy;
        searchValue = this.dummyMapDoc[i][0].searchValue;
        doctor = this.dummyMapDoc[i][0].doctor.userCode;
        var dosDataFrom = this.dummyMapDoc[i][j].dosFrom.getMonth() + 1;
        var dosDataTo = this.dummyMapDoc[i][j].dosTo.getMonth() + 1;
        documentTypes.push(this.dummyMapDoc[i][j].documentTypes);
        pageNumber.push(this.dummyMapDoc[i][j].pageNumber);
        dosFrom.push(this.dummyMapDoc[i][j].dosFrom.getFullYear() + '-' + String(dosDataFrom).padStart(2, '0') + '-' + String(this.dummyMapDoc[i][j].dosFrom.getDate()).padStart(2, '0'));
        dosTo.push(this.dummyMapDoc[i][j].dosTo.getFullYear() + '-' + String(dosDataTo).padStart(2, '0') + '-' + String(this.dummyMapDoc[i][j].dosTo.getDate()).padStart(2, '0'))
      }
      this.getDataMapDoc.push({ "documentTypes": documentTypes, "pageNumber": pageNumber, "dosFrom": dosFrom, "dosTo": dosTo, "patientCode": patientCode, "practiceCode": practice, "searchBy": searchBy, "searchValue": searchValue, "doctorCode": doctor })
    }
    console.log(this.getDataMapDoc);
    let formvalue = JSON.stringify(this.getDataMapDoc);
    this.formD.append('FormValue', formvalue);
    this.formD.append('File', this.formD.get('Document'));
    alert(pageTotalCount+ " pages has been selected.");
    //this.mapDocService.create()
    //return;
    this.mapDocService.create(this.formD).subscribe(data => {
      this.toastrManager.successToastr('Map doc Creation', 'Map Doc Created Successfully.');
      this.aboutToSave = true;
      this.router.navigate(['mdmuser/patients']);
    }, error => {
      console.log(error);
      this.toastrManager.successToastr('Map doc Creation', 'Map Doc Created Successfully.');
      this.aboutToSave = true;
      this.router.navigate(['mdmuser/patients']);
    });
    // console.log("IN Save...Document is: . :", this.formD.get("Document"));
    // //console.log("IN Save... FormValues are : ", formvalue);
    // // let newmodel = (this.model);
    // // let id = this.model.patientItem;
    // // this.storagemodel.push(newmodel);
    // //localStorage.setItem('mapdocarray', JSON.stringify(this.mapDocArray))
    // console.log('data array is ', this.storagemodel);
    // //localStorage.setItem('model', JSON.stringify(this.storagemodel));
    // console.log('patientId is ', this.model.patientItem);
    // let lcstrge = JSON.parse(localStorage.getItem('model'));
    // console.log(' data from local storage is ' + lcstrge);
    // //this.mapDocService.create(this.formD);
    // console.log('sent data to Create method of mapdocservice');

    this.form.get('documentTypes').reset();
    this.form.get('practice').reset();
    this.form.get('dosFrom').reset();
    this.form.get('dosTo').reset();
    this.form.get('searchBy').reset();
    this.form.get('searchValue').reset();
    this.form.get('patientItem').reset();
    this.form.get('doctor').reset();
    //  this.form.get('dob').reset();
    //  this.form.get('doa').reset();
    this.form.get('pageNumber').reset();

    // localStorage.setItem("reloadCount", '0');
    // this.delay(4000).then(any => {
    //   this.router.navigate(['/mdmuser/mapDocuments', id])

    // });

    this.formD.delete('FormValue');
    this.model.documentTypes = [];
    this.model.dosFrom = [];
    this.model.dosTo = [];
    this.model.pageNumber = [];
    this.model.practice = null;
    this.model.searchBy = '';
    this.model.patientItem = ''
    this.model.searchValue = '';
    this.model.doctor = null;
    //this.model=null;
    //this.model.doa=null;
    //this.model.dob=null;
    this.remainingPdfPages = [];
    this.remainingPdfPages = new Array(this.totalPages);
    this.dataSource = null;
    this.mapAddDoc = [];

  }
  incrementPage(amount: number) {
    this.page += amount;
    this.currentPage=this.remainingPdfPages[this.page-1];
  }

  showCurrentPage(amount: number) {
    this.page = amount;
    this.currentPage=this.remainingPdfPages[this.page-1];
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("wait for the option to select"));
  }


  selectPage(amount: number) {
    //  this.selectPageNo = amount.toString();
    // this.pageNumber = amount.toString();
    // console.log('page no. are ' + this.selectPageNo);
    //  this.mySelect.open();
    console.log(this.pageNumber)
    var amountNumber = amount;
    console.log(this.remainingPdfPages[amountNumber - 1]);
    if (this.pageNumber.length > 0) {
      console.log(this.pageNumber.indexOf(amount.toString()))
      if (this.pageNumber.indexOf(amount.toString()) === -1) {
        this.pageNumber.push(amount.toString());
        console.log(amountNumber - 1)
        this.remainingPdfPages[amountNumber - 1] = false;
      }
      console.log(this.pageNumber)
      // if(this.remainingPdfPages[amountNumber]==false){
      //   this.pageNumber.splice(this.pageNumber.indexOf(amount.toString()),1);
      //   this.remainingPdfPages[amountNumber - 1] = true;
      // }
    } else {
      this.pageNumber.push(amount.toString());
      console.log(amountNumber - 1)
      this.remainingPdfPages[amountNumber - 1] = false;
    }
    console.log(this.remainingPdfPages);
    this.currentPage = false;
  }

  deselectPage(amount) {
    console.log(this.pageNumber);
    console.log(amount);
    console.log(this.pageNumber.indexOf(amount.toString()))
    this.pageNumber.splice(this.pageNumber.indexOf(amount.toString()), 1);
    this.remainingPdfPages[amount - 1] = true;
    this.currentPage = true;
  }

  selectPageNext() {
    this.selectPageNo = this.page.toString();
    // this.mySelect.open();
    this.delay(2000).then(any => {
      this.incrementPage(1);
    })
  }

  openedChange(opened: boolean) {
    if (opened === true && this.selectPageNo) {
      document.getElementById(this.selectPageNo).click();
      console.log('page no. are ' + this.selectPageNo);
      this.selectPageNo = '';
      this.opened = opened;
      this.delay(1000).then(any => {
        this.mySelect.close();
      });
    }
    return true;
  }

  SelectAll(ev) {
    console.log(this.remainingPdfPages)
    console.log(typeof this.remainingPdfPages)
    for (let i = 0; i < this.remainingPdfPages.length; i++) {
      if (this.remainingPdfPages[i] !== null) {
        console.log('in if')
        this.remainingPdfPages[i] = i + 1;
      }
    }
    var dataStrings = this.remainingPdfPages.map(function (value) {
      return String(value);
    });
    let result;
    result = dataStrings.filter(s => s !== 'null');
    this.pageNumber = result;
  }

  unSelectAll() {
    this.form.controls.pageNumber.patchValue([]);
  }

  afterLoadComplete(pdf: PDFDocumentProxy) {
    this.pdf = pdf;
    this.isLoaded = true;
    this.totalPages = pdf.numPages;
    this.totalPDFPages = pdf.numPages;
    this.remainingPdfPages = [];
    // this.remainingPdfPages = new Array(pdf.numPages);
    // console.log(pdf.numPages);
    for (var i = 0; i < pdf.numPages; i++) {
      this.remainingPdfPages.push(true);
    }
    console.log(this.remainingPdfPages);
    this.currentPage = this.remainingPdfPages[0];
    this.loadOutline();
  }

  loadOutline() {
    this.pdf.getOutline().then((outline: any[]) => {
      this.outline = outline;
    });
  }
  pageRendered(e: CustomEvent) {
    console.log('(page-rendered)', e);
  }

  toggleChange(value: any) {
    console.log('Value is ', value);
  }
  editMapDoc(mapDetails, index, value: any) {
    console.log('in editmapdoc value', value);
    console.log('mapdetails', mapDetails);
    console.log(this.rowedData);





    this.deletedisabled = true;
    this.addAndUpdate = false;
    this.mapDetails = mapDetails;
    this.isActive = true;
    this.index = index;
    this.pastindex = index;
    this.documentTypes = mapDetails.documentTypes;
    this.dosFrom = mapDetails.dosFrom;
    this.dosTo = mapDetails.dosTo;
    this.patientItem = mapDetails.patientItem;
    this.practice = mapDetails.practice;
    this.doctor = mapDetails.doctor;
    var indexValue = 0;
    console.log(index);
    console.log(this.rowedData[index]);
    for (var j = 0; j < this.rowedData.length; j++) {
      if (index == j) {
        for (var i = 0; i < this.rowedData[index].pageNumber.length; i++) {
          this.remainingPdfPages[this.rowedData[index].pageNumber[i] - 1] = false;
        }
      } else {
        for (var xx = 0; xx < this.rowedData[j].pageNumber.length; xx++) {
          if (mapDetails.patientCode === this.rowedData[j].patientItem.patientCode) {
            this.remainingPdfPages[this.rowedData[j].pageNumber[xx] - 1] = null;
          }

        }
      }
    }
    this.pageNumber = this.rowedData[index].pageNumber;
    console.log(this.remainingPdfPages)
    // for (var i = 0; i < this.rowedData.length; i++) {
    //   if (this.rowedData[i].patientItem.patientCode == mapDetails.patientCode) {
    //     indexValue = 1;
    //     if (indexValue == 0) {
    //       for (var j = 0; j < this.rowedData[i].pageNumber.length; j++) {
    //         this.remainingPdfPages[parseInt(this.rowedData[i].pageNumber[j]) - 1] = false;
    //       }
    //     }
    //     if (indexValue == 0) {
    //       for (var j = 0; j < this.rowedData[i].pageNumber.length; j++) {
    //         this.remainingPdfPages[parseInt(this.rowedData[i].pageNumber[j]) - 1] = null;
    //       }
    //     }
    //   }
    // }

    // if (typeof mapDetails.pageNumber == 'string') {
    //   let page_number = ((mapDetails.pageNumber).split(','));
    //   this.pageNumber = page_number;
    // }
    // else {
    //   let page_number = mapDetails.pageNumber;
    //   this.pageNumber = page_number;
    // }

    // for (let i = 0; i < this.pageNumber.length; i++) {
    //   let j = this.pageNumber[i];
    //   if (this.remainingPdfPages[j - 1] == null) {
    //     this.remainingPdfPages[j - 1] = this.pageNumber[i];
    //   }
    // }
    this.editdisabled = true;
  }
  Cancel() {
    this.isActive = false;
    this.editdisabled = false;
    this.addAndUpdate = true;
    this.deletedisabled = false;
    this.documentTypes = this.mapDetails.documentTypes;
    this.dosFrom = this.mapDetails.dosFrom;
    this.dosTo = this.mapDetails.dosTo;
    this.remainingPdfPages = [];
    this.pageNumber = [];
    // this.remainingPdfPages = new Array(pdf.numPages);
    // console.log(pdf.numPages);
    for (var i = 0; i < this.totalPDFPages; i++) {
      this.remainingPdfPages.push(true);
    }
    // if (typeof this.mapDetails.pageNumber == 'string') {
    //   let page_number = ((this.mapDetails.pageNumber).split(','));
    //   this.pageNumber = page_number;
    // }
    // else {
    //   let page_number = this.mapDetails.pageNumber;
    //   this.pageNumber = page_number;
    // }
    // for (let i = 0; i < this.remainingPdfPages.length; i++) {
    //   let j = this.pageNumber[i];
    //   this.remainingPdfPages[j - 1] = null;
    // }


    // this.form.get('practice').reset();
    this.form.get('documentTypes').reset();
    this.form.get('patientItem').reset();
    this.form.get('dosFrom').reset();
    this.form.get('dosTo').reset();
    this.form.get('pageNumber').reset();


  }
  updateMapDoc() {
    if (this.pageNumber.length > 0) {
      console.log(this.index);
      this.editdisabled = false;
      this.deletedisabled = false;
      this.addAndUpdate = true;
      let formvalue = this.form.value;
      let page_number = ((this.pageNumber).join(','));
      this.rowedData.push({ "documentTypes": this.documentTypes, "dosFrom": this.dosFrom, "dosTo": this.dosTo, "pageNumber": this.pageNumber, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": this.searchBy, "searchValue": this.searchValue, "patientItem": this.patientItem });
      this.temp.push({ "documentTypes": this.documentTypes, "dosFrom": this.dosFrom, "dosTo": this.dosTo, "pageNumber": page_number, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": this.searchBy, "searchValue": this.searchValue, "patientItem": this.patientItem });
      this.mapAddDoc[this.index] = this.temp[0];
      this.temp = [];
      //this.form.get('practice').reset();
      this.form.get('documentTypes').reset();
      this.form.get('dosFrom').reset();
      this.form.get('dosTo').reset();
      this.form.get('patientItem').reset();
      this.form.get('pageNumber').reset();
      this.reinitializeData(this.mapAddDoc);
      this.remainingPdfPages = [];
      this.pageNumber = [];
      // this.remainingPdfPages = new Array(pdf.numPages);
      // console.log(pdf.numPages);
      for (var i = 0; i < this.totalPDFPages; i++) {
        this.remainingPdfPages.push(true);
      }
    } else {
      this.toastrManager.errorToastr("Pages are not added", "No Pages");
    }

    // console.log('in update ')
    // this.editdisabled = false;
    // this.deletedisabled = false;
    // this.isActive = false;
    // //this.temp.push({ "documentTypes":this.documentTypes,"dosFrom": this.dosFrom, "dosTo": this.dosTo, "pageNumber": this.pageNumber, "practice":this.practice, "doctor":this.doctor, "searchBy": this.searchBy, "searchValue": this.searchValue,"patientItem": this.patientItem, "doa": this.doa, "dob":this.dob } );
    // let formvalue = this.form.value;
    // // this.temp.push({ "documentTypes": this.documentTypes, "dosFrom": this.dosFrom, "dosTo": this.dosTo, "pageNumber": this.pageNumber, "practice": formvalue.practice.practiceName, "doctor": formvalue.doctor.firstName, "searchBy": this.searchBy, "searchValue": this.searchValue, "patientItem": this.patientItem });
    // this.temp.push({ "documentTypes": this.documentTypes, "dosFrom": this.dosFrom, "dosTo": this.dosTo, "pageNumber": this.pageNumber, "practice": formvalue.practice, "doctor": formvalue.doctor, "searchBy": this.searchBy, "searchValue": this.searchValue, "patientItem": this.patientItem });
    // this.mapAddDoc[this.index] = this.temp[0];
    // this.temp = [];
    // this.reinitializeData(this.mapAddDoc);
    // this.addAndUpdate = true;
    // this.model.documentTypes[this.index] = this.documentTypes;
    // this.dosFormatDate(this.dosFrom, this.dosTo);
    // this.model.dosFrom[this.index] = this.newdosFrom;
    // this.model.dosTo[this.index] = this.newdosTo;
    // this.model.pageNumber[this.index] = this.pageNumber.join(',');
    // console.log('after update data is ');
    // console.log(this.model);
    // for (let i = 0; i < this.remainingPdfPages.length; i++) {
    //   let j = this.pageNumber[i];
    //   this.remainingPdfPages[j - 1] = null;
    // }
    // this.delay(500).then(any => {
    //   // this.form.get('practice').reset();
    //   this.form.get('documentTypes').reset();
    //   this.form.get('dosFrom').reset();
    //   this.form.get('dosTo').reset();
    //   this.form.get('pageNumber').reset();
    // });
  }

  deleteMapDoc(mapDetails, index) {
    this.addAndUpdate = true;
    // let page_no: any;
    // page_no = (this.model.pageNumber[index]);
    // let pageno = page_no.split(',');
    // for (let i = 0; i < pageno.length; i++) {
    //   let j = pageno[i];
    //   if (this.remainingPdfPages[j - 1] == null) {
    //     this.remainingPdfPages[j - 1] = page_no[i];
    //   }
    // }
    this.rowedData.splice(index, 1);
    this.mapAddDoc.splice(index, 1);
    this.reinitializeData(this.mapAddDoc);
    this.pageNumber = [];
    // this.model.documentTypes.splice(index, 1);
    // this.model.dosFrom.splice(index, 1);
    // this.model.dosTo.splice(index, 1);
    // this.model.pageNumber.splice(index, 1);
    // console.log('after delete data is');
    // console.log(this.model);

    // this.form.get('practice').reset();
    this.form.get('documentTypes').reset();
    this.form.get('dosFrom').reset();
    this.form.get('dosTo').reset();
    this.form.get('pageNumber').reset();

    if (this.mapAddDoc.length == 0) {


      this.form.get('searchBy').reset();
      this.form.get('searchValue').reset();
      this.form.get('patientItem').reset();
      // this.form.get('dob').reset();
      // this.form.get('doa').reset();

      this.dataSource = null;
      this.mapAddDoc = [];
    }
  }
}
