import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { PracticeService } from '../services/practice.service';
import { AddpracticeComponent } from '../addpractice/addpractice.component';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'PracticeName', 'Address', 'City', 'Tasks'];
  dataSource: any;
  searchString:any;
  constructor(public toastrManager: ToastrManager, public dialog: MatDialog, private route: ActivatedRoute, public practiceService: PracticeService,
    private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem("USER_TOKEN")) {
      this.practiceService.getAllPractice().subscribe(data => {
        console.log(data);
        for (var index = 0; index < data.length; index++) {
          data[index].zipCode = data[index].zipCode + '';
          var zipCodeData = '';
          for (var i = 0; i < data[index].zipCode.length; i++) {
            zipCodeData = zipCodeData + data[index].zipCode[i];
            if (i == 4) {
              zipCodeData = zipCodeData + '-';
            }
          }
          data[index].zipCode = zipCodeData;
        }

        console.log(data);
        this.dataSource = data;
      })
    } else {
      this.router.navigate(['']);
    }
  }

  searchPractice(){
    console.log(this.searchString);
    this.dataSource = [];
    this.practiceService.searchPractice(this.searchString).subscribe(data=>{
      console.log(data);
      for (var index = 0; index < data.length; index++) {
        data[index].zipCode = data[index].zipCode + '';
        var zipCodeData = '';
        for (var i = 0; i < data[index].zipCode.length; i++) {
          zipCodeData = zipCodeData + data[index].zipCode[i];
          if (i == 4) {
            zipCodeData = zipCodeData + '-';
          }
        }
        data[index].zipCode = zipCodeData;
      }

      console.log(data);
      this.dataSource = data;
    },error=>{
      this.toastrManager.errorToastr("Something went wrong (or) no records found.","Error.")
    })
  }

  reinitializeData() {
    this.practiceService.getAllPractice().subscribe(data => {
      console.log(data);
      for (var index = 0; index < data.length; index++) {
        data[index].zipCode = data[index].zipCode + '';
        var zipCodeData = '';
        for (var i = 0; i < data[index].zipCode.length; i++) {
          zipCodeData = zipCodeData + data[index].zipCode[i];
          if (i == 4) {
            zipCodeData = zipCodeData + '-';
          }
        }
        data[index].zipCode = zipCodeData;
      }

      console.log(data);
      this.dataSource = data;
    })
  }

  openAddPracticeDialog(practice?) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.panelClass = 'addPracticeForm';
    dialogConfig.id = 'practiceForm';

    dialogConfig.data = practice;
    // dialogConfig.data={id: practice.id}

    const dialogRef = this.dialog.open(AddpracticeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(response => this.reinitializeData());
  }
  // applyFilter(filterValue: string) {
  //   console.log(this.dataSource);
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  deletePractice(id) {
    this.practiceService.deletePractice(id).subscribe(data => {
      this.toastrManager.successToastr('Deleted Practice', 'Deleted Practice Successfully');
    })
  }

}
