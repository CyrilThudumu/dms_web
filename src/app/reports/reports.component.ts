import { Component, OnInit, Inject, Optional, Input, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

//import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT, formatDate } from '@angular/common';
import { first, isEmpty } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
//import { AlertService } from './../_services/alert.service';
//import { Practice } from './../_models/practice';
//import { PracticeService } from './../practice.service';
import { AttroneyService } from '../services/attroney.service';
import { PatientService } from '../services/patient.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Form, Validators, FormGroup, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { PracticeService } from '../services/practice.service';
//import { doc } from '../_models/doc';
//import { attorneydos } from '../_models/attorneydos';
//import { User } from '../_models';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  practice:any;
  status:any;
  attorney:any;
  fromDate:any;
  toDate:any;
  newpractice:any;
  user:any;
  loader:boolean=false;
  statusDateChangeValue=false;
  displayedColumns = ['patientId', 'Name', 'attorney', 'doctor', 'claimNo', 'policyNo', 'doa', 'Tasks'];
  constructor(private practiceService:PracticeService,private authenticationService:AuthenticationService) {
    this.practiceService.getAllPractice().subscribe(data => {
      this.newpractice = data;
      // this.patientService.getAllPatients().subscribe(patientData=>{
      //   this.dataSource = patientData;
      // })
      console.log(this.newpractice);
    });
    this.authenticationService.getAllUsers().subscribe(data => {
      this.user = data.filter(userValue => {
        console.log(userValue.authorities.indexOf("ROLE_ATTORNEY"));
        return userValue.authorities.indexOf("ROLE_ATTORNEY") > -1;
      });
    })
   }

  ngOnInit() {
  }

  fromDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.statusDateChangeValue = false;
    }else{
      this.statusDateChangeValue = true;
    }
    console.log(this.fromDate)
  }

  toDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.statusDateChangeValue = false;
    }else{
      this.statusDateChangeValue = true;
    }
  }

  submitSearchReport(){
    let searchData = {
      "practice":this.practice,
      "user":this.user,
      "attorney":this.attorney,
      "fromDate":this.fromDate,
      "toDate":this.toDate
    }
  }
}
