import { Component, OnInit, Inject, Optional, Input, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

//import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT, formatDate } from '@angular/common';
import { first, isEmpty } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
//import { AlertService } from './../_services/alert.service';
//import { Practice } from './../_models/practice';
//import { PracticeService } from './../practice.service';
import { AttroneyService } from '../services/attroney.service';
import { PatientService } from '../services/patient.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Form, Validators, FormGroup, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-editdocumentfromattorney',
  templateUrl: './editdocumentfromattorney.component.html',
  styleUrls: ['./editdocumentfromattorney.component.css']
})
export class EditdocumentfromattorneyComponent implements OnInit {
  dos;
  user: any[] = []
  form: FormGroup;
  pdata: any;
  index: number;
  doctype:any;
  formD: FormData
  pdfSrc;
  filename: string;
  onAdd = new EventEmitter();
  // status: string[]=[];
  status: string;
  statusDate: any;
  attorney: any;
  // attorney:User[]=[];
  // paymentDate: Date[]=[];
  paymentDate: any;
  arbNotes: any;
  natureOfDispute: any;
  attdos: any;
  saveDisabled: boolean;
  getPatietDocs: any;
  statusDateChangeValue=false;
  paymentDateChangeValue=false;
  sentDateChangeValue = false;
  arbAmount:any;
  paymentAmount:any;
  sentDate:any;
  currentDetails:any;
  constructor(
    //private nwdata:attorneydos,
    // @Inject(DOCUMENT) private document: any,
    private patientService: PatientService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public toastrManager: ToastrManager,
    public attroneyService: AttroneyService,
    public authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<EditdocumentfromattorneyComponent>,
    //private practiceService : PracticeService,
    // private alertService :AlertService,
    @Inject(MAT_DIALOG_DATA) @Optional() data?: any,
  ) {
    console.log(data.doc[data.index].docLink[0])
    this.form = fb.group({
      dos: data.doc[data.index].docLink[0].appendDate,
      attorney: data.doc[data.index].docLink[0].attorneyCode,
      statusDate: data.doc[data.index].docLink[0].statusDate,
      status: data.doc[data.index].docLink[0].status,
      paymentDate: data.doc[data.index].docLink[0].paymentDate,
      arbNotes: data.doc[data.index].docLink[0].arbNotes,
      natureOfDispute: data.doc[data.index].docLink[0].natureOfDispute,
      arbAmount: data.doc[data.index].docLink[0].arbAmount,
      paymentAmount: data.doc[data.index].docLink[0].paymentAmount,
      sentDate: data.doc[data.index].docLink[0].sentDate,
    });

    this.authenticationService.getUserDetails(localStorage.getItem('USER_TOKEN')).subscribe(loggedInUsser => {
      console.log(loggedInUsser);
      this.currentDetails = loggedInUsser;
      this.dos=data.doc[data.index].docLink[0].appendDate;
    this.attorney= this.currentDetails.firstName+this.currentDetails.lastName;
      this.statusDate= data.doc[data.index].docLink[0].statusDate;
      this.status= data.doc[data.index].docLink[0].status;
      this.paymentDate= data.doc[data.index].docLink[0].paymentDate;
      this.arbNotes= data.doc[data.index].docLink[0].arbNotes;
      this.natureOfDispute= data.doc[data.index].docLink[0].natureOfDispute;
      this.arbAmount= data.doc[data.index].docLink[0].arbAmount;
      this.paymentAmount= data.doc[data.index].docLink[0].paymentAmount;
      this.sentDate= data.doc[data.index].docLink[0].sentDate;
    })

    

    //console.log(data);
    // attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
    //this.attdos = this.patientService.getattorneydos(data.doc.patientItem)||[];
    this.dos = data.dos;
    this.pdata = data;
    this.index = data.index;
    this.getPatietDocs = data.patientData
    //this.user=this.patientService.getUser('attorney');
    this.authenticationService.getAllUsers().subscribe(data => {
      this.user = data.filter(userValue => {
        console.log(userValue.authorities.indexOf("ROLE_ATTORNEY"));
        return userValue.authorities.indexOf("ROLE_ATTORNEY") > -1;
      });
    })
    console.log(this.user);
    console.log(this.attdos);
    //this.status=this.patientr.status;
    // this.statusDate=this.patientr.statusDate; 
    //  this.attorney=this.patientr.attorney[data.index];
    //  console.log('attorney is '+ this.patientr.attorney[data.index])
    //  this.paymentDate=this.patientr.paymentDate;
    //  this.arbNotes=this.patientr.arbNotes;
    //  this.natureOfDispute=this.patientr.natureOfDispute;
    //if(data.doc.attorney){


    // this.status=data.doc.status;
    // this.statusDate=data.doc.statusDate; 


    // console.log(this.attdos.attorney);
    // if(this.attdos.attorney){

    // this.attorneyobj=data.doc.attorney[data.index];
    // this.attorney=data.doc.attorney[data.index];
    //  this.saveDisabled=true;
    // console.log(this.attdos.attorney);
    //  }
    //   else {
    // this.attorneyobj={};
    //  this.attorney={};
    //    this.saveDisabled=false;
    // }
    //if(data.hasOwnProperty('attorney'))
    if (data.doc.hasOwnProperty('attorney')/* && data.doc.attorney[this.index]*/) {
      console.log('has property attorney')
      console.log('index is ', this.index)
      let formvalue;
      formvalue = this.form.value;

      // this.status[this.pdata.index]=data.doc.status[this.pdata.index];
      this.status = data.doc.status[this.pdata.index];




      // this.statusDate[this.pdata.index]=data.doc.statusDate[this.pdata.index] ; 
      this.statusDate = data.doc.statusDate[this.pdata.index];
      //this.statusDate[this.pdata.index]=formvalue.statusDate ; 
      this.paymentDate = data.doc.paymentDate[this.pdata.index];
      //this.paymentDate[this.pdata.index] =formvalue.paymentDate ;
      this.arbNotes[this.pdata.index] = data.doc.arbNotes[this.pdata.index];
      this.natureOfDispute[this.pdata.index] = data.doc.natureOfDispute[this.pdata.index];
      this.attorney = data.doc.attorney[this.pdata.index];
      console.log('attorney is ', this.attorney)
      if (!this.attorney) {
        this.saveDisabled = false;
      }
      else
        this.saveDisabled = true;
      console.log(this.paymentDate)
      console.log(this.statusDate)
    }
    //console.log(this.attdos.hasOwnProperty('attorney'))
    console.log('attorney is');
    console.log(this.attorney);

    console.log('index is');
    console.log(this.index);
    console.log('data is');
    console.log(data);
    //  this.paymentDate=data.doc.paymentDate;
    //  this.arbNotes=data.doc.arbNotes;
    //  this.natureOfDispute=data.doc.natureOfDispute;
    //}
  }

  ngOnInit() {
    // if(!this.pdata)
    // this.pdata={};
  }
  compareFn(user1: any, user2: any) {
    return user1 && user2 ? user1.id === user2.id : user1 === user2;
  }
  handleFileInput(event: any) {
    event.preventDefault();
    console.log('in handle file input');
    // this.pdfSrc=this.pdata.pdfSrc;  
  }

  statusDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.statusDateChangeValue = false;
    }else{
      this.statusDateChangeValue = true;
    }
  }

  paymentDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.paymentDateChangeValue = false;
    }else{
      this.paymentDateChangeValue = true;
    }
  }

  sentDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.sentDateChangeValue = false;
    }else{
      this.sentDateChangeValue = true;
    }
  }

  

  save() {
    console.log('in save before dos mapping ', this.pdata.doc); //this.nwdata
    let formvalue = this.form.value;
    console.log(formvalue);
    console.log(this.pdata);
    console.log(this.getPatietDocs);
    var docValue = this.pdata.dos.split(" ");
    console.log(this.statusDateChangeValue);
    var statusMonth;
    var sentDateMonth;
    var paymentDateMonth;
    if(this.statusDateChangeValue){
      statusMonth = this.form.value.statusDate.getMonth() + 1;
    }else{
      statusMonth=''
    }
    if(this.sentDateChangeValue){
      sentDateMonth = this.form.value.sentDate.getMonth() + 1;
    }else{
      sentDateMonth=''
    }
    if(this.paymentDateChangeValue){
      paymentDateMonth = this.form.value.paymentDate.getMonth() + 1;
    }else{
      paymentDateMonth=''
    }
   
   console.log(this.form.value  )
    
    var newCode = {
      "status": this.form.value.status,
      "statusDate": !statusMonth?'':this.form.value.statusDate.getFullYear() + '-' + String(statusMonth).padStart(2, '0') + '-' + String(this.form.value.statusDate.getDate()).padStart(2, '0'),
      "arbAmount": this.form.value.arbAmount,
      "sentDate": !sentDateMonth?'':this.form.value.sentDate.getFullYear() + '-' + String(sentDateMonth).padStart(2, '0') + '-' + String(this.form.value.sentDate.getDate()).padStart(2, '0'),
      "paymentAmount": this.form.value.paymentAmount,
      "paymentDate": !paymentDateMonth?'':this.form.value.paymentDate.getFullYear() + '-' + String(paymentDateMonth).padStart(2, '0') + '-' + String(this.form.value.paymentDate.getDate()).padStart(2, '0'),
      "arbNotes": this.form.value.arbNotes,
      "natureOfDispute": this.form.value.natureOfDispute,
      "attorneyCode": formvalue.attorney.firstName + formvalue.attorney.lastName,
      "patientCode": this.getPatietDocs.patientCode,
      "fromDos": docValue[0],
      "toDos": docValue[2]
    }
    this.formD = new FormData()
    this.formD.append('FormValue', JSON.stringify(newCode));
    this.formD.append('File', '');
    this.attroneyService.assignAttroney(newCode).subscribe(data => {
      this.toastrManager.successToastr("Attorney Assigned", "Success");
    }, error => {
      this.toastrManager.errorToastr("Attorney Assigned not Completed", "Failure");
    })
    // this.nwdata.natureOfDispute[this.pdata.index]=formvalue.natureOfDispute;
    // this.nwdata.arbNotes[this.pdata.index]=formvalue.arbNotes;
    // this.nwdata.paymentDate[this.pdata.index]=formvalue.paymentDate;
    // this.nwdata.statusDate[this.pdata.index]=formvalue.statusDate;
    // this.nwdata.status[this.pdata.index]=formvalue.status;
    // this.nwdata.patientItem=this.pdata.doc.patientItem;
    // this.nwdata.practice=this.pdata.doc.practice;
    // this.nwdata.dosFrom=this.pdata.doc.dosFrom;
    //  this.nwdata.dosTo=this.pdata.doc.dosTo;
    //  this.nwdata.pageNumber=this.pdata.doc.pageNumber;
    //  this.nwdata.documentTypes=this.pdata.doc.documentTypes;
    // // this.nwdata.dosNewFrom=this.pdata.doc.dosNewFrom;
    // // this.nwdata.dosNewTo=this.pdata.doc.dosNewTo;
    //  this.nwdata.newDocType=this.pdata.doc.newDocType;
    //  this.nwdata.doctor=this.pdata.doc.doctor;
    //    this.nwdata.attorney[this.pdata.index]=(this.form.get('attorney').value);
    //  //  console.log('data, nwdata after dos mapping ', this.pdata.doc, this.nwdata);     
    //     this.onAdd.emit(this.nwdata);

    this.dialogRef.close();
  }
  // update(){
  // this.patientService.updatedos(this.nwdata)
  //}
  close() {
    this.dialogRef.close();
  }

}
