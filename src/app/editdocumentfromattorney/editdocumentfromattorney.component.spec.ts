import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditdocumentfromattorneyComponent } from './editdocumentfromattorney.component';

describe('EditdocumentfromattorneyComponent', () => {
  let component: EditdocumentfromattorneyComponent;
  let fixture: ComponentFixture<EditdocumentfromattorneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditdocumentfromattorneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditdocumentfromattorneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
