import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdituserpatientComponent } from './edituserpatient.component';

describe('EdituserpatientComponent', () => {
  let component: EdituserpatientComponent;
  let fixture: ComponentFixture<EdituserpatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdituserpatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdituserpatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
