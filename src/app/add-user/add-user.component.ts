import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AuthenticationService } from '../services/authentication.service';
import { PracticeService } from '../services/practice.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  userData: any;
  changeTitle: boolean;
  disAbled = false;
  submitted = false;
  constructor(private practiceService: PracticeService, private dialogRef: MatDialogRef<AddUserComponent>, public authenticationService: AuthenticationService, private toastrManager: ToastrManager, @Inject(MAT_DIALOG_DATA) @Optional() data?: any) {
    
    if (data) {
      this.changeTitle = true;
      console.log(data);
      if (data.authorities[0] == "ROLE_ADMIN") {
        data.role = "admin";
      }
      if (data.authorities[0] == "ROLE_ATTORNEY") {
        data.role = "attorney";
      }
      if (data.authorities[0] == "ROLE_DOCTOR") {
        data.role = "doctor";
      }
      if (data.authorities[0] == "ROLE_USER") {
        data.role = "mdmuser";
      }
      if(data.practice== "doctor"){
        this.disAbled = true;
      }
      this.userData = {
        id: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        username: data.email,
        practice: data.practice,
        role: data.role,
        practicec: ''
      }
    } else {
      this.changeTitle = false;
      console.log(data)
      this.userData = {
        id: '',
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        practice: '',
        role: '',
        practicec: ''

      }
    }
  }

  ngOnInit() {
    this.practiceService.getAllPractice().subscribe(dataValue => {
      console.log(dataValue);
      this.userData.practices = dataValue;
      console.log(this.userData)
    })
  }
  onRoleChange(value) {
    if (value === 'doctor') {
      console.log(this.disAbled);
      this.disAbled = true;
      console.log(this.disAbled);
    } else {
      this.disAbled = false;
    }
  }

  roleChange(roleName) {
    console.log(roleName);
    if (roleName == "doctor") {
      this.disAbled = true;
    } else {
      this.disAbled = false;
    }
  }

  save() {
    if (!this.userData.firstName) {
      this.toastrManager.errorToastr('First Name Required', 'Missing Required Field!');
      return false;
    }
    
    if (!this.userData.username) {
      this.toastrManager.errorToastr('Username Required', 'Missing Required Field!');
      return false;
    }
    if (!this.userData.id) {
      if (!this.userData.password) {
        this.toastrManager.errorToastr('Password Required', 'Missing Required Field!');
        return false;
      }
    }

    if (!this.userData.role) {
      this.toastrManager.errorToastr('Role Required', 'Missing Required Field!');
      return false;
    }
    // if (!this.userData.practice) {
    //   this.toastrManager.errorToastr('Practice Required', 'Missing Required Field!');
    //   return false;
    // }
    this.submitted = true;
    // stop here if form is invalid
    if (this.userData.id) {
      this.authenticationService.editUser(this.userData).subscribe(data => {
        this.dialogRef.close();
        this.toastrManager.successToastr('User Creation', 'User Created Successfully.')
      }, error => {
        console.log(error);
        this.toastrManager.errorToastr(error.error.message, 'User Error.')
      });
    } else {
      this.authenticationService.createUser(this.userData).subscribe(data => {
        console.log(data)
        this.dialogRef.close();
        this.toastrManager.successToastr('User Creation', 'User Created Successfully.')
      }, error => {
        console.log(error);
        this.toastrManager.errorToastr(error.error.message, 'User Error.')
      }
      );
    }
    console.log(this.userData);
  }
  close() {
    this.dialogRef.close();
  }

}
