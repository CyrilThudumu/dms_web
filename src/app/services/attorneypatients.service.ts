import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttorneypatientsService {

  constructor(private http: HttpClient) { }

  getAttorneyPatients(callDetails){
    //http://localhost:8080/api/patientsAttorney/MA/VCarve;
    //http://localhost:8080/api/patientsAttorney/fidan
    //return this.http.get<any>(environment.apiUrl+'/api/patientsAttorney/'+callDetails.attorney+'/'+callDetails.practiceCode,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
    return this.http.get<any>(environment.apiUrl+'/api/patientsAttorney/'+callDetails.attorney,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }

  getAttorneyHistoryPatients(callDetails){
    //http://localhost:8080/api/patientsProcessedByAttorney/CyrilAttorney
    return this.http.get<any>(environment.apiUrl+'/api/patientsProcessedByAttorney/'+callDetails.attorney,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }

  getDownloads(detailValues){
    //http://localhost:8080/api/documentsload/{patientCode}/{practiceCode}/{fromDos}/{toDos}
    return this.http.get<any>(environment.apiUrl+'/api/documentsload/'+detailValues.patientCode+'/'+detailValues.practiceCode+'/'+detailValues.fromDos+'/'+detailValues.toDos,{headers:{"Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }

  getDoctorPatients(callDetails){
    //http://localhost:8080/api/patientsDoctor/{doctorCode}
    return this.http.get<any>(environment.apiUrl+'/api/patientsDoctor/'+callDetails.doctor,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
}
