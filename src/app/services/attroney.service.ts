import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttroneyService {

  constructor(private http: HttpClient) { }

  assignAttroney(attroneyData){
    console.log(attroneyData);
    return this.http.put<any>(environment.apiUrl+'/api/documents', attroneyData,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }

  processedData(processedData){
    return this.http.put<any>(environment.apiUrl+'/api/documentsAttorneyProcessed', processedData,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
}
