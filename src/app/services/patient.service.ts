import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  getPatientsbyattorney: any;
  users: any = [];
  user: any = [];
  selectpatients: any;
  constructor(private http: HttpClient) { }
  getAllPatients() {
    return this.http.get<any>(environment.apiUrl + '/api/patients', { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }
  getAPatients(id){
    return this.http.get<any>(environment.apiUrl + '/api/patients/'+id, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }

  getCodePatients(practiceCode) {
    //http://localhost:8080/api/patientsByPractice/{practiceCode}
    return this.http.get<any>(environment.apiUrl + '/api/patientsByPractice/'+practiceCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }
  getSearchDoctorPatients(practiceCode) {
    //http://localhost:8080/api/patientsForDoctor/John/CY
    return this.http.get<any>(environment.apiUrl + '/api/patientsForDoctor/'+practiceCode.doctor+'/'+practiceCode.searchString, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }
  getSearchAttorneyPatients(practiceCode) {
    //http://localhost:8080/api/patientsForAttorney/{attorneyCode}/{searchKey}
    return this.http.get<any>(environment.apiUrl + '/api/patientsForAttorney/'+practiceCode.attorney+'/'+practiceCode.searchString, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }

  createPatient(patientData) {
    console.log(patientData)
    return this.http.post<any>(environment.apiUrl + '/api/patients', patientData, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
    //return ;
  }

  updatePatient(patientData) {
    console.log(patientData)
    return this.http.put<any>(environment.apiUrl + '/api/patients', patientData, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
    //return ;
  }

  getPatientDOS(patientCode, pracriceCode) {
    //for ex: http://localhost:8080/api/documentsByPat/MDM369258
    return this.http.get<any>(environment.apiUrl + '/api/documentsByPat/' + patientCode + '/' + pracriceCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }
  getAttorneyHistoryDOS(attorneyCode, patientCode) {
    //http://localhost:8080/api/documentsByAttorneyProcessed/CyrilAttorney/MXC202022
    return this.http.get<any>(environment.apiUrl + '/api/documentsByAttorneyProcessed/' + attorneyCode + '/' + patientCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }

  getAttorneyDOS(patientCode, attorneyCode) {
    //http://localhost:8080/api/documentsByAttorney/MashaFidan/EVR101010
    //for ex: http://localhost:8080/api/documentsByPat/MDM369258
    return this.http.get<any>(environment.apiUrl + '/api/documentsByAttorney/' + attorneyCode + '/' + patientCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }

  getDoctorDOS(patientCode, doctorCode){
    //http://localhost:8080/api/documentsByDoc/{doctorCode}/{patientCode}
    return this.http.get<any>(environment.apiUrl + '/api/documentsByDoc/' + doctorCode + '/' + patientCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }

  getPatients() {
    if (!localStorage.patients) {
      localStorage.patients = JSON.stringify([]);
    }
    return JSON.parse(localStorage.patients);
  }
  getPatientsByPractice(selectedPractice): any {
    this.selectpatients = [];
    // throw new Error("Method not implemented.");
    console.log('selectedPractice', selectedPractice)
    let patient = this.getPatients();
    let j = 0;
    for (let i = 0; i < patient.length; i++) {
      if (selectedPractice === patient[i].practice) {
        console.log('prac is ', patient[i].practice)
        this.selectpatients[j] = patient[i]
        j++;
      }

    }
    console.log(' selected patients by practice ', this.selectpatients)
    return this.selectpatients;

  }

  getUser(role): any {
    let j = 0;
    let k = 0;
    let l = 0;

  }

  getPatientByIdfrdis(PatientId: any) {
    console.log('in patient service id is and mapdoc are', PatientId);
    // this.practices= this.Practiceservice.getPractices();

    let j = 0;
    let patientR;
    let dummyArray;
    // if(patientId==this.patientobj[i])

    // for (let i = 0; i < PatientId.length; i++) {
    patientR = PatientId;
    // patientR = PatientId[i];
    // if(PatientId == patientR.patientItem.patientId){

    // this.patientobj[j] = patientR;
    // console.log(this.patients);
    console.log(patientR);
    dummyArray = patientR;
    dummyArray.newDocType = [];
    var newDoctypes = [];
    var newPageNumbers = [];
    for (var newIndex = 0; newIndex < patientR.dosFrom.length; newIndex++) {
      if (newIndex === 0) {
        dummyArray.newDocType.push({ dosDate: patientR.dosFrom[newIndex] + ' To ' + patientR.dosTo[newIndex] });
      }

      else {
        for (var index = 0; index < dummyArray.newDocType.length; index++) {
          if (dummyArray.newDocType[index].dosDate === patientR.dosFrom[newIndex] + ' To ' + patientR.dosTo[newIndex]) {
          } else {
            dummyArray.newDocType.push({ dosDate: patientR.dosFrom[newIndex] + ' To ' + patientR.dosTo[newIndex] });
            break;
          }
        }
      }

    }
    console.log(dummyArray.newDocType)
    let unique = dummyArray.newDocType.map(item => item.dosDate).filter((value, index, self) => self.indexOf(value) === index)
    // = [...new Set(dummyArray.newDocType.map(item => item.dosDate))];
    console.log(unique);
    dummyArray.newDocType = [];
    for (var index = 0; index < unique.length; index++) {
      dummyArray.newDocType.push({ dosDate: unique[index] })
    }

    for (var newIndex = 0; newIndex < dummyArray.newDocType.length; newIndex++) {
      for (var index = 0; index < patientR.dosFrom.length; index++) {

        if (dummyArray.newDocType[newIndex].dosDate === patientR.dosFrom[index] + ' To ' + patientR.dosTo[index]) {
          newDoctypes.push(patientR.documentTypes[index]);
          newPageNumbers.push(patientR.pageNumber[index]);
        }
      }
      dummyArray.newDocType[newIndex].pageNumber = newPageNumbers;
      dummyArray.newDocType[newIndex].docType = newDoctypes; //.push({ dosFrom: patientR.dosFrom[newIndex], dosTo: patientR.dosTo[newIndex],docType: });
      newDoctypes = [];
      newPageNumbers = [];
    }

    console.log(dummyArray);
    //  if(this.currentuser.role==='admin' || this.currentuser.role==='mdmuser')
    if (localStorage.getItem('USER_ROLE') === 'admin' || localStorage.getItem('USER_ROLE') === 'mdmuser')
      return dummyArray;

    // i=this.model.length;
    // j++;


    // console.log(this.patientobj);
    // return this.patientobj;
  }

  demographicDoc(codeDetails){
    //http://localhost:8080/api/patientdemographic/{practiceCode}/{patientCode}
    console.log(codeDetails)
    return this.http.get<any>(environment.apiUrl + '/api/patientdemographic/' + codeDetails.practiceCode + '/' + codeDetails.patientCode, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('USER_TOKEN') } });
  }
}
