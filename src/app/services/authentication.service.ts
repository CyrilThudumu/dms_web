import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  dataValues:any;
  createUserData:any;
  constructor(private http: HttpClient) { }
  login(username:any,password:any){
    console.log(username);
    console.log(password);
    this.dataValues = {
      "username": username, "password": password
    }
    console.log(this.dataValues);
    return this.http.post<any>(environment.apiUrl+'/api/authenticate', {username: username, password: password})
  }
  getUserDetails(token){
    return this.http.get<any>(environment.apiUrl+'/api/account', {headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
  getAllUsers(){
    return this.http.get<any>(environment.apiUrl+'/api/users', {headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
  getSearchUsers(searchString){
    //http://localhost:8080/api/userSearch/{searchKey}
    return this.http.get<any>(environment.apiUrl+'/api/userSearch/'+searchString, {headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
  createUser(userData){
    console.log(userData);
    var arrayRole; 
        if(userData.role=="admin"){
          arrayRole = ["ROLE_ADMIN"];
        }
        if(userData.role=="attorney"){
          arrayRole = ["ROLE_ATTORNEY"];
        }
        if(userData.role=="doctor"){
          arrayRole = ["ROLE_DOCTOR"];
        }
        if(userData.role=="mdmuser"){
          arrayRole = ["ROLE_USER"];
        }
    
    //{"firstName":"cyril","lastName":"thudumu","login":"cyrilmdmusr@test.com","password":"cyriladmin","practice":"","email":"cyrilmdmusr@test.com","authorities":["ROLE_USER"]}
    this.createUserData = {
      "firstName": userData.firstName,
      "lastName": userData.lastName,
      "password": userData.password,
      "practice": userData.practice,
      "authorities": arrayRole,
      "login": userData.username,
      "email":userData.username,
      "userCode":userData.userCode
    }
    console.log(this.createUserData)
    return this.http.post<any>(environment.apiUrl+'/api/users', this.createUserData,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
  editUser(userData){
    console.log(userData);
    var arrayRole; 
        if(userData.role=="admin"){
          arrayRole = ["ROLE_ADMIN"];
        }
        if(userData.role=="attorney"){
          arrayRole = ["ROLE_ATTORNEY"];
        }
        if(userData.role=="doctor"){
          arrayRole = ["ROLE_DOCTOR"];
        }
        if(userData.role=="mdmuser"){
          arrayRole = ["ROLE_USER"];
        }
    
    //{"firstName":"cyril","lastName":"thudumu","login":"cyrilmdmusr@test.com","password":"cyriladmin","practice":"","email":"cyrilmdmusr@test.com","authorities":["ROLE_USER"]}
    this.createUserData = {
      "id":userData.id,
      "firstName": userData.firstName,
      "lastName": userData.lastName,
      "password": userData.password,
      "practice": userData.practice,
      "authorities": arrayRole,
      "login": userData.username,
      "email":userData.username,
      "userCode":userData.userCode
    }
    console.log(this.createUserData)
    return this.http.put<any>(environment.apiUrl+'/api/users', this.createUserData,{headers:{"Content-Type": "application/json","Authorization":"Bearer "+ localStorage.getItem('USER_TOKEN')}})
  }
}
