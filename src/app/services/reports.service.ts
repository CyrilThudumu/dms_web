import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http: HttpClient) { }

  searchForReports(searchString){
    return this.http.get<any>(environment.apiUrl + '/api/practice/'+searchString, { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } })
  }
}
