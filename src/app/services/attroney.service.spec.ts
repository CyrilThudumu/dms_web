import { TestBed } from '@angular/core/testing';

import { AttroneyService } from './attroney.service';

describe('AttroneyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttroneyService = TestBed.get(AttroneyService);
    expect(service).toBeTruthy();
  });
});
