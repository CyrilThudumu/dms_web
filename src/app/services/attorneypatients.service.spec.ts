import { TestBed } from '@angular/core/testing';

import { AttorneypatientsService } from './attorneypatients.service';

describe('AttorneypatientsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttorneypatientsService = TestBed.get(AttorneypatientsService);
    expect(service).toBeTruthy();
  });
});
