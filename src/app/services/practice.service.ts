import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PracticeService {

  constructor(private http: HttpClient) { }
  getAllPractice() {
    return this.http.get<any>(environment.apiUrl + '/api/practices', { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } })
  }

  searchPractice(searchString){
    return this.http.get<any>(environment.apiUrl + '/api/practice/'+searchString, { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } })
  }
  createPractice(practiceData) {
    practiceData.zipcode = practiceData.zipcode.replace(/-/g, '');
    console.log(practiceData);
    //return;
    const newPractice = {
      "practiceCode": practiceData.practiceCode,
      "practiceName": practiceData.practiceName,
      "address1": practiceData.address1,
      "address2": practiceData.address2?practiceData.address2:'',
      "city": practiceData.city,
      "stateProvince": practiceData.state,
      "tinNumber":practiceData.tinnum,
      "zipCode": parseInt(practiceData.zipcode), "userId": localStorage.getItem("USER_ID"), "userLogin": localStorage.getItem('USER_ROLE')
    };
    console.log(newPractice)
    return this.http.post<any>(environment.apiUrl+'/api/practices', newPractice, { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } });
  }
  editPractice(practiceData){
    practiceData.zipcode = practiceData.zipcode.replace(/-/g, '');
    console.log(practiceData);
    //return;
    const newPractice = {
      "id":practiceData.id,
      "practiceCode": practiceData.practiceCode,
      "practiceName": practiceData.practiceName,
      "address1": practiceData.address1,
      "address2": practiceData.address2?practiceData.address2:'',
      "city": practiceData.city,
      "stateProvince": practiceData.state,
      "tinNumber":practiceData.tinnum,
      "zipCode": parseInt(practiceData.zipcode), "userId": localStorage.getItem("USER_ID"), "userLogin": localStorage.getItem('USER_ROLE')
    };
    console.log(newPractice)
    return this.http.put<any>(environment.apiUrl+'/api/practices', newPractice, { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } });
  }
  deletePractice(id){
    const newPractice = {
      "id":id,
    }
    return this.http.delete<any>(environment.apiUrl+'/api/practices?id='+id, { headers: { "Content-Type": "application/json", "Authorization": "Bearer " + localStorage.getItem('USER_TOKEN') } });
  }
}``
