import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatProgressSpinnerModule, MatMenuModule, MatIconModule, MatToolbarModule, MatDividerModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatSortModule, MatTableModule, MatPaginatorModule, MatDialogModule, MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldControl, MAT_DATE_LOCALE, MatSlideToggleModule, MatListModule, MatExpansionModule, MatSidenavModule,MatCheckboxModule } from '@angular/material';
import { AdminComponent } from './admin/admin.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddpracticeComponent } from './addpractice/addpractice.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UserpatientComponent } from './userpatient/userpatient.component';
import { AddUserPatientComponent } from './add-user-patient/add-user-patient.component';
import { MapdocsComponent } from './mapdocs/mapdocs.component';
import { MapDocumnetsComponent } from './map-documnets/map-documnets.component';
import { DoctorComponent } from './doctor/doctor.component';
import { AttorneyComponent } from './attorney/attorney.component';
import { EdituserpatientComponent } from './edituserpatient/edituserpatient.component';
import { DisplayPatientDocsComponent } from './display-patient-docs/display-patient-docs.component';
import { AssigndocstoattroneyComponent } from './assigndocstoattroney/assigndocstoattroney.component';
import { AttorneypatientsComponent } from './attorneypatients/attorneypatients.component';
import { DocumentfromattorneyComponent } from './documentfromattorney/documentfromattorney.component';
import { EditdocumentfromattorneyComponent } from './editdocumentfromattorney/editdocumentfromattorney.component';
import { LoaderComponent } from './loader/loader.component';
import { EditUserPatientComponent } from './edit-user-patient/edit-user-patient.component';
import { DoctorpatientsComponent } from './doctorpatients/doctorpatients.component';
import { DocumentfromdoctorComponent } from './documentfromdoctor/documentfromdoctor.component';
import { SearchattorneypatientComponent } from './searchattorneypatient/searchattorneypatient.component';
import { AttorneyHistoryComponent } from './attorney-history/attorney-history.component';
import { DoctorHistoryComponent } from './doctor-history/doctor-history.component';
import { AttorneyDosHistoryComponent } from './attorney-dos-history/attorney-dos-history.component';
import { ReportsComponent } from './reports/reports.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    AdminComponent,
    AddpracticeComponent,
    AdminUserComponent,
    AddUserComponent,
    UserpatientComponent,
    AddUserPatientComponent,
    MapdocsComponent,
    MapDocumnetsComponent,
    //MapdocsComponent
    DoctorComponent,
    AttorneyComponent,
    EdituserpatientComponent,
    DisplayPatientDocsComponent,
    AssigndocstoattroneyComponent,
    AttorneypatientsComponent,
    DocumentfromattorneyComponent,
    EditdocumentfromattorneyComponent,
    LoaderComponent,
    EditUserPatientComponent,
    DoctorpatientsComponent,
    DocumentfromdoctorComponent,
    SearchattorneypatientComponent,
    AttorneyHistoryComponent,
    DoctorHistoryComponent,
    AttorneyDosHistoryComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MatSidenavModule,
    NgbModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDividerModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatListModule,
    MatExpansionModule,
    PdfViewerModule,
    CommonModule,
    MatSidenavModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[
    AddpracticeComponent,AddUserComponent,AssigndocstoattroneyComponent,EditdocumentfromattorneyComponent
  ]
})
export class AppModule { }
