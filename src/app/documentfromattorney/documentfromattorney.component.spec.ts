import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentfromattorneyComponent } from './documentfromattorney.component';

describe('DocumentfromattorneyComponent', () => {
  let component: DocumentfromattorneyComponent;
  let fixture: ComponentFixture<DocumentfromattorneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentfromattorneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentfromattorneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
