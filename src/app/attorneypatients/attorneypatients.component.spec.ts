import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneypatientsComponent } from './attorneypatients.component';

describe('AttorneypatientsComponent', () => {
  let component: AttorneypatientsComponent;
  let fixture: ComponentFixture<AttorneypatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneypatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneypatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
