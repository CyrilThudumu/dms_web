import { Component, OnInit } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { PatientService } from '../services/patient.service';
import { PracticeService } from '../services/practice.service';
import { AttorneypatientsService } from '../services/attorneypatients.service';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-attorneypatients',
  templateUrl: './attorneypatients.component.html',
  styleUrls: ['./attorneypatients.component.css']
})
export class AttorneypatientsComponent implements OnInit {
  dataSource: any;
  newpractice: any;
  currentDetails:any;
  searchString:any;
  loader=false;
  attorneyName:any;
  displayedColumns = ['patientId', 'Name', 'attorney', 'doctor', 'claimNo', 'policyNo', 'doa', 'Tasks'];
  constructor(public toastrManager:ToastrManager,public authenticationService:AuthenticationService,public attorneypatientsService:AttorneypatientsService,public practiceService: PracticeService, public dialog: MatDialog, public patientService: PatientService,
    private router: Router) { 
      if (localStorage.getItem('USER_TOKEN')) {
        this.authenticationService.getUserDetails(localStorage.getItem('USER_TOKEN')).subscribe(loggedInUsser => {
          this.currentDetails = loggedInUsser;
          console.log(this.currentDetails)
          this.attorneyName = this.currentDetails.userCode;
          this.attorneypatientsService.getAttorneyPatients({"attorney":this.currentDetails.userCode}).subscribe(patientData=>{
            console.log('@#$#@$#@$@#$@#$$@#$#@$$#@$#@$')
            console.log(patientData);
            this.dataSource = patientData;
            if(!this.dataSource){
              this.toastrManager.errorToastr("No records found.","Error!")
            }
          })
        });
        this.practiceService.getAllPractice().subscribe(data => {
          this.newpractice = data;
          // this.patientService.getAllPatients().subscribe(patientData=>{
          //   this.dataSource = patientData;
          // })
          console.log(this.newpractice);
        })
      } else {
        this.router.navigate(['']);
      }
    }

  ngOnInit() {
  }

  getPatients(practice){
    console.log(practice);
    this.authenticationService.getUserDetails(localStorage.getItem('USER_TOKEN')).subscribe(loggedInUsser => {
      this.currentDetails = loggedInUsser;
      console.log(this.currentDetails)
      this.attorneypatientsService.getAttorneyPatients({"practiceCode":practice,"attorney":this.currentDetails.userCode}).subscribe(patientData=>{
        console.log(patientData);
        this.dataSource = patientData;
        if(!this.dataSource){
          this.toastrManager.errorToastr("No records found.","Error!")
        }
      })
    });
    
  }
  navigateDosPage(patientCode,practiceCode){
    console.log(patientCode)
    this.router.navigate(['mdmuser/docsfromattorney/'+patientCode+'/'+practiceCode])
  }

  demographicCode(patientCode,practiceCode){
    this.loader=true;
    console.log(patientCode);
    console.log(practiceCode);
    this.patientService.demographicDoc({"practiceCode":practiceCode,"patientCode":patientCode}).subscribe(data=>{
      console.log(data);
      console.log(data.fileContent);
    
    var dataURI = "data:application/pdf;base64," + data.fileContent;
    //window.open(dataURI);  
    //var string = doc.output('datauristring');
    var iframe = "<iframe width='100%' height='100%' src='" + dataURI + "'></iframe>"
    var x = window.open();
    x.document.open();
    x.document.write(iframe);
    x.document.close();
    this.loader=false;
    });
    //console.log(demographoc)
  }
  searchingString(){
    // var practiceCode;
    // console.log(this.practice);
    // console.log(this.newpractice)
    // for(var i=0;i<this.newpractice.length;i++){
    //   //console.log(this.newpractice[i].id)
    //   if(this.practice == this.newpractice[i].id){
    //     practiceCode = this.newpractice[i].practiceCode;
    //   }
    // }
    // console.log(practiceCode)
    // this.patientService.getCodePatients(practiceCode).subscribe(patientData=>{
    //   this.dataSource = patientData;
    // })
    this.dataSource = [];
    this.patientService.getSearchAttorneyPatients({"attorney":this.attorneyName,"searchString":this.searchString}).subscribe(data=>{
      this.dataSource = data;
    },error=>{
      this.toastrManager.errorToastr("Something went wrong (or) no records found.","Error.")
    })

  }
}
