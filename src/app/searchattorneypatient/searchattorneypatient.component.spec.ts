import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchattorneypatientComponent } from './searchattorneypatient.component';

describe('SearchattorneypatientComponent', () => {
  let component: SearchattorneypatientComponent;
  let fixture: ComponentFixture<SearchattorneypatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchattorneypatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchattorneypatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
