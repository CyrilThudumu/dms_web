import { AddUserComponent } from './../add-user/add-user.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {
  dataSource:any;
  searchString:any;
  displayedColumns = ['id', 'Name', 'Email', 'Practice', 'Role', 'Tasks'];
  constructor(public dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private router: Router) { 
      if (localStorage.getItem('USER_TOKEN')) {
        this.authenticationService.getAllUsers().subscribe(data=>{
          console.log(data)
          this.dataSource = data;
        })
      }else{
        this.router.navigate(['']);
      }
    }

  ngOnInit() {
  }

  searchUser(){
    this.dataSource = [];
    this.authenticationService.getSearchUsers(this.searchString).subscribe(data=>{
      console.log(data)
      this.dataSource = data;
    })
  }

  reinitializeData(){
    this.authenticationService.getAllUsers().subscribe(data=>{
      console.log(data)
      this.dataSource = data;
    })
  } 
  openAddUserDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.panelClass = 'addUserForm';
    

    //dialogConfig.data = user;
    // dialogConfig.data={id: practice.id}

    const dialogRef = this.dialog.open(AddUserComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(response => this.reinitializeData());
  }

}
