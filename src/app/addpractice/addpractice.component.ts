import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormBuilder, Form, Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { PracticeService } from '../services/practice.service';

@Component({
  selector: 'app-addpractice',
  templateUrl: './addpractice.component.html',
  styleUrls: ['./addpractice.component.css']
})
export class AddpracticeComponent implements OnInit {
  practiceData: any;
  changeTitle: boolean;

  constructor(public practiceService: PracticeService, public toastrManager: ToastrManager, private dialogRef: MatDialogRef<AddpracticeComponent>, @Inject(MAT_DIALOG_DATA) @Optional() data?: any) {

    if (data) {
      this.changeTitle = true;
      console.log(data);
      data.zipCode = data.zipCode + '';
      var zipCodeData = '';
      for (var i = 0; i < data.zipCode.length; i++) {
        zipCodeData = zipCodeData + data.zipCode[i];
        if (i == 4) {
          zipCodeData = zipCodeData + '-';
        }
      }
      data.zipCode = zipCodeData;
      //console.log(zipCodeData);
      this.practiceData = {
        id: data.id,
        practiceName: data.practiceName,
        practiceCode: data.practiceCode,
        address1: data.address1,
        address2:data.address2,
        city: data.city,
        state: data.stateProvince,
        zipcode: data.zipCode,
        tinnum: data.tinNumber
      }
    } else {
      this.changeTitle = false;
      console.log(data)
      this.practiceData = {
        id: '',
        practiceName: '',
        practiceCode: '',
        address1: '',
        address2:'',
        city: '',
        state: '',
        zipcode: '',
        tinnum: ''
      }
    }
  }

  ngOnInit() {
  }
  zipcodeValidate(valueZip, eventVal) {
    console.log(valueZip);
    if (valueZip.charCodeAt(valueZip.length - 1) >= 48 && valueZip.charCodeAt(valueZip.length - 1) <= 57) {
      if (valueZip.length === 5) {
        this.practiceData.zipcode = valueZip + '-';
        //this.form.get('zipcode').setValue(valueZip + '-');
      }
    } else {
      this.practiceData.zipcode = '';//valueZip.substr(valueZip.length - 1);
    }
    // console.log(valueZip.length);

    // if (valueZip.length > 10) {
    //   console.log('sdfkjshdfjksdhf');
    //   console.log(valueZip.substr(0, valueZip.length - 1))
    //   this.practiceData.zipcode = valueZip.substr(0, valueZip.length - 1);
    //   //this.form.get('zipcode').setValue(valueZip.substr(0, valueZip.length - 1));
    // } else {
    //   console.log(valueZip.charCodeAt(valueZip.length - 1));
    //   if (valueZip.charCodeAt(valueZip.length - 1) >= 48 && valueZip.charCodeAt(valueZip.length - 1) <= 57) {
    //     if (valueZip.length === 5) {
    //       this.practiceData.zipcode = valueZip + '-';
    //       //this.form.get('zipcode').setValue(valueZip + '-');
    //     }
    //   } else {
    //     var retrunvalue = valueZip.substr(0, valueZip.length - 1)
    //     console.log(retrunvalue)
    //     valueZip = '';
    //     this.practiceData.zipcode = valueZip;
    //     //this.practiceData.zipcode = valueZip.substr(0, valueZip.length - 1);
    //     //this.form.get('zipcode').setValue(valueZip.substr(0, valueZip.length - 1));
    //   }
    // }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if(this.practiceData.tinnum.length>10){

      this.practiceData.tinnum = this.practiceData.tinnum.substr(0, this.practiceData.tinnum.length - 1)
      //this.form.get('sAdjusterfax').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
    return true;

  }
  save(practice) {
    if (!practice.practiceName) {
      this.toastrManager.errorToastr('Practice name is required.', 'Field Missing!');
      return false;
    }
    if (!practice.practiceCode) {
      this.toastrManager.errorToastr('Practice code is required.', 'Field Missing!');
      return false;
    }
    if (!practice.address1) {
      this.toastrManager.errorToastr('Address First Line is required.', 'Field Missing!');
      return false;
    }
    if (!practice.city) {
      this.toastrManager.errorToastr('City is required.', 'Field Missing!');
      return false;
    }
    if (!practice.state) {
      this.toastrManager.errorToastr('State is required.', 'Field Missing!');
      return false;
    }
    if (!practice.zipcode) {
      this.toastrManager.errorToastr('Zincode is required.', 'Field Missing!');
      return false;
    }

    if (practice.id) {
      this.practiceService.editPractice(practice).subscribe(data => {
        this.dialogRef.close();
        this.toastrManager.successToastr('Practice Updation', 'Practice Updated Successfully.')
      },error=>{
        console.log(error);
        this.toastrManager.errorToastr(error.error.message, 'User Error.')
      });
    } else {
      console.log(practice);
      this.practiceService.createPractice(practice).subscribe(data => {
        this.dialogRef.close();
        this.toastrManager.successToastr('Practice Creation', 'Practice Created Successfully.')
      },error=>{
        console.log(error);
        this.toastrManager.errorToastr(error.error.message, 'User Error.')
      });

    }
  }

  close() {
    this.dialogRef.close();
  }
}
