import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyHistoryComponent } from './attorney-history.component';

describe('AttorneyHistoryComponent', () => {
  let component: AttorneyHistoryComponent;
  let fixture: ComponentFixture<AttorneyHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
