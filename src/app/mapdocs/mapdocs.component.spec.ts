import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapdocsComponent } from './mapdocs.component';

describe('MapdocsComponent', () => {
  let component: MapdocsComponent;
  let fixture: ComponentFixture<MapdocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapdocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapdocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
