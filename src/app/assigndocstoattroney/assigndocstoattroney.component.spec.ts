import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssigndocstoattroneyComponent } from './assigndocstoattroney.component';

describe('AssigndocstoattroneyComponent', () => {
  let component: AssigndocstoattroneyComponent;
  let fixture: ComponentFixture<AssigndocstoattroneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigndocstoattroneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigndocstoattroneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
