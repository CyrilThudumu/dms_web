import { Component, OnInit, Inject, Optional, Input, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

//import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT, formatDate } from '@angular/common';
import { first, isEmpty } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
//import { AlertService } from './../_services/alert.service';
//import { Practice } from './../_models/practice';
//import { PracticeService } from './../practice.service';
import { AttroneyService } from '../services/attroney.service';
import { PatientService } from '../services/patient.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Form, Validators, FormGroup, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
//import { doc } from '../_models/doc';
//import { attorneydos } from '../_models/attorneydos';
//import { User } from '../_models';

@Component({
  selector: 'app-assigndocstoattroney',
  templateUrl: './assigndocstoattroney.component.html',
  styleUrls: ['./assigndocstoattroney.component.css']
})
export class AssigndocstoattroneyComponent implements OnInit {

  dos;
  doctype='';
  user: any[] = []
  form: FormGroup;
  pdata: any;
  index: number;
  formD: FormData
  pdfSrc;
  filename: string;
  onAdd = new EventEmitter();
  // status: string[]=[];
  status: string;
  statusDate: any;
  attorney: any;
  // attorney:User[]=[];
  // paymentDate: Date[]=[];
  paymentDate: any;
  arbNotes: any;
  natureOfDispute: any;
  attdos: any;
  saveDisabled: boolean;
  getPatietDocs: any;
  statusDateChangeValue=false;
  paymentDateChangeValue=false;
  sentDateChangeValue = false;
  paymentAmount:any;
  arbAmount:any;
  sentDate:any;
  constructor(
    //private nwdata:attorneydos,
    // @Inject(DOCUMENT) private document: any,
    private patientService: PatientService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public toastrManager: ToastrManager,
    public attroneyService: AttroneyService,
    public authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<AssigndocstoattroneyComponent>,
    //private practiceService : PracticeService,
    // private alertService :AlertService,
    @Inject(MAT_DIALOG_DATA) @Optional() data?: any,
  ) {
    this.form = fb.group({
      dos: ['', Validators.required],
      attorney: ['', Validators.required],
      statusDate: ['', Validators.required],
      status: ['', Validators.required],
      paymentDate: ['', Validators.required],
      arbNotes: ['', Validators.required],
      natureOfDispute: ['', Validators.required],
      arbAmount: ['', Validators.required],
      paymentAmount: ['', Validators.required],
      sentDate: ['', Validators.required],
    })

    //console.log(data);
    // attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
    //this.attdos = this.patientService.getattorneydos(data.doc.patientItem)||[];
    this.dos = data.dos;
    this.pdata = data;
    this.index = data.index;
    this.getPatietDocs = data.patientData
    //this.user=this.patientService.getUser('attorney');
    this.authenticationService.getAllUsers().subscribe(data => {
      this.user = data.filter(userValue => {
        console.log(userValue.authorities.indexOf("ROLE_ATTORNEY"));
        return userValue.authorities.indexOf("ROLE_ATTORNEY") > -1;
      });
    })
    console.log(this.user);
    console.log(this.attdos);
    //this.status=this.patientr.status;
    // this.statusDate=this.patientr.statusDate; 
    //  this.attorney=this.patientr.attorney[data.index];
    //  console.log('attorney is '+ this.patientr.attorney[data.index])
    //  this.paymentDate=this.patientr.paymentDate;
    //  this.arbNotes=this.patientr.arbNotes;
    //  this.natureOfDispute=this.patientr.natureOfDispute;
    //if(data.doc.attorney){


    // this.status=data.doc.status;
    // this.statusDate=data.doc.statusDate; 


    // console.log(this.attdos.attorney);
    // if(this.attdos.attorney){

    // this.attorneyobj=data.doc.attorney[data.index];
    // this.attorney=data.doc.attorney[data.index];
    //  this.saveDisabled=true;
    // console.log(this.attdos.attorney);
    //  }
    //   else {
    // this.attorneyobj={};
    //  this.attorney={};
    //    this.saveDisabled=false;
    // }
    //if(data.hasOwnProperty('attorney'))
    if (data.doc.hasOwnProperty('attorney')/* && data.doc.attorney[this.index]*/) {
      console.log('has property attorney')
      console.log('index is ', this.index)
      let formvalue;
      formvalue = this.form.value;

      // this.status[this.pdata.index]=data.doc.status[this.pdata.index];
      this.status = data.doc.status[this.pdata.index];




      // this.statusDate[this.pdata.index]=data.doc.statusDate[this.pdata.index] ; 
      this.statusDate = data.doc.statusDate[this.pdata.index];
      //this.statusDate[this.pdata.index]=formvalue.statusDate ; 
      this.paymentDate = data.doc.paymentDate[this.pdata.index];
      //this.paymentDate[this.pdata.index] =formvalue.paymentDate ;
      this.arbNotes[this.pdata.index] = data.doc.arbNotes[this.pdata.index];
      this.natureOfDispute[this.pdata.index] = data.doc.natureOfDispute[this.pdata.index];
      this.attorney = data.doc.attorney[this.pdata.index];
      console.log('attorney is ', this.attorney)
      if (!this.attorney) {
        this.saveDisabled = false;
      }
      else
        this.saveDisabled = true;
      console.log(this.paymentDate)
      console.log(this.statusDate)
    }
    //console.log(this.attdos.hasOwnProperty('attorney'))
    console.log('attorney is');
    console.log(this.attorney);

    console.log('index is');
    console.log(this.index);
    console.log('data is');
    console.log(data);
    //  this.paymentDate=data.doc.paymentDate;
    //  this.arbNotes=data.doc.arbNotes;
    //  this.natureOfDispute=data.doc.natureOfDispute;
    //}
  }

  ngOnInit() {
    // if(!this.pdata)
    // this.pdata={};
  }
  compareFn(user1: any, user2: any) {
    return user1 && user2 ? user1.id === user2.id : user1 === user2;
  }
  handleFileInput(event: any) {
    event.preventDefault();
    console.log('in handle file input');
    // this.pdfSrc=this.pdata.pdfSrc;  
  }

  statusDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.statusDateChangeValue = false;
    }else{
      this.statusDateChangeValue = true;
    }
  }

  paymentDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.paymentDateChangeValue = false;
    }else{
      this.paymentDateChangeValue = true;
    }
  }

  sentDateChange(event){
    console.log(!event.value)

    if(!event.value){
      this.sentDateChangeValue = false;
    }else{
      this.sentDateChangeValue = true;
    }
  }

  

  save() {
    console.log('in save before dos mapping ', this.pdata.doc); //this.nwdata
    let formvalue = this.form.value;
    console.log(formvalue);
    console.log(this.pdata);
    console.log(this.getPatietDocs);
    var docValue = this.pdata.dos.split(" ");
    console.log(this.statusDateChangeValue);
    var statusMonth;
    var sentDateMonth;
    var paymentDateMonth;
    if(this.statusDateChangeValue){
      statusMonth = this.form.value.statusDate.getMonth() + 1;
    }else{
      statusMonth=''
    }
    if(this.sentDateChangeValue){
      sentDateMonth = this.form.value.sentDate.getMonth() + 1;
    }else{
      sentDateMonth=''
    }
    if(this.paymentDateChangeValue){
      paymentDateMonth = this.form.value.paymentDate.getMonth() + 1;
    }else{
      paymentDateMonth=''
    }
   
   console.log(this.form.value);
   //return;
    var nameAttorney = formvalue.attorney.userCode
    var newCode = {
      "status": !this.form.value.status?'':this.form.value.status,
      "statusDate": !statusMonth?'':this.form.value.statusDate.getFullYear() + '-' + String(statusMonth).padStart(2, '0') + '-' + String(this.form.value.statusDate.getDate()).padStart(2, '0'),
      "arbAmount": !this.form.value.arbAmount?'':this.form.value.arbAmount,
      "sentDate": !sentDateMonth?'':this.form.value.sentDate.getFullYear() + '-' + String(sentDateMonth).padStart(2, '0') + '-' + String(this.form.value.sentDate.getDate()).padStart(2, '0'),
      "paymentAmount": !this.form.value.paymentAmount?'':this.form.value.paymentAmount,
      "paymentDate": !paymentDateMonth?'':this.form.value.paymentDate.getFullYear() + '-' + String(paymentDateMonth).padStart(2, '0') + '-' + String(this.form.value.paymentDate.getDate()).padStart(2, '0'),
      "arbNotes": !this.form.value.arbNotes?'':this.form.value.arbNotes,
      "natureOfDispute": !this.form.value.natureOfDispute?'':this.form.value.natureOfDispute,
      "attorneyCode": !nameAttorney?'':nameAttorney,
      "patientCode": !this.getPatietDocs.patientCode?'':this.getPatietDocs.patientCode,
      "fromDos": !docValue[0]?'':docValue[0],
      "toDos": !docValue[2]?'':docValue[2]
    }
    this.formD = new FormData()
    this.formD.append('FormValue', JSON.stringify(newCode));
    this.formD.append('File', '');
    this.attroneyService.assignAttroney(newCode).subscribe(data => {
      this.toastrManager.successToastr("Attorney Assigned", "Success");
    }, error => {
      this.toastrManager.errorToastr("Attorney Assigned not Completed", "Failure");
    })
    // this.nwdata.natureOfDispute[this.pdata.index]=formvalue.natureOfDispute;
    // this.nwdata.arbNotes[this.pdata.index]=formvalue.arbNotes;
    // this.nwdata.paymentDate[this.pdata.index]=formvalue.paymentDate;
    // this.nwdata.statusDate[this.pdata.index]=formvalue.statusDate;
    // this.nwdata.status[this.pdata.index]=formvalue.status;
    // this.nwdata.patientItem=this.pdata.doc.patientItem;
    // this.nwdata.practice=this.pdata.doc.practice;
    // this.nwdata.dosFrom=this.pdata.doc.dosFrom;
    //  this.nwdata.dosTo=this.pdata.doc.dosTo;
    //  this.nwdata.pageNumber=this.pdata.doc.pageNumber;
    //  this.nwdata.documentTypes=this.pdata.doc.documentTypes;
    // // this.nwdata.dosNewFrom=this.pdata.doc.dosNewFrom;
    // // this.nwdata.dosNewTo=this.pdata.doc.dosNewTo;
    //  this.nwdata.newDocType=this.pdata.doc.newDocType;
    //  this.nwdata.doctor=this.pdata.doc.doctor;
    //    this.nwdata.attorney[this.pdata.index]=(this.form.get('attorney').value);
    //  //  console.log('data, nwdata after dos mapping ', this.pdata.doc, this.nwdata);     
    //     this.onAdd.emit(this.nwdata);

    this.dialogRef.close();
  }
  // update(){
  // this.patientService.updatedos(this.nwdata)
  //}
  close() {
    this.dialogRef.close();
  }

}
