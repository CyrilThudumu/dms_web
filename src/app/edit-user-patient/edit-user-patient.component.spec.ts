import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserPatientComponent } from './edit-user-patient.component';

describe('EditUserPatientComponent', () => {
  let component: EditUserPatientComponent;
  let fixture: ComponentFixture<EditUserPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
