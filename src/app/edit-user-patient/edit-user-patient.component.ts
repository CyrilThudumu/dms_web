//import { PatientService } from './../patient.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
// import { Practice } from '../_models/practice';
import { PracticeService } from '../services/practice.service';
import { PatientService } from '../services/patient.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-edit-user-patient',
  templateUrl: './edit-user-patient.component.html',
  styleUrls: ['./edit-user-patient.component.css']
})
export class EditUserPatientComponent implements OnInit {
  practice:any;
  form: FormGroup;
  newpractice: any;
  filename: string;
  formD: FormData;
  pdfSrc;
  demo: boolean;
  insuranceInfo: boolean;
  conditionalAOB: boolean;
  exportNoteReport: boolean;
  policeReport: boolean;
  progressNote: boolean;
  refferal: boolean;
  attorneyInfo: boolean;
  noFaultApplication: boolean;
  mrcctScanResults: boolean;
  affiInsurnace: boolean;
  superBill: boolean;
  policyDeclaration: boolean;
  acupConsent: boolean;
  nf2nf3Forms: boolean;
  aob: boolean;
  batchDate: any;
  dos: any;
  dataForEdit: any;
  patientData: any;
  constructor(public patientService: PatientService,
    private fb: FormBuilder, private activatedRoute: ActivatedRoute,
    private practiceService: PracticeService,
    private router: Router,
    private toastrManager: ToastrManager
  ) {

    if (localStorage.getItem('USER_TOKEN')) {
      this.demo = false;
      this.insuranceInfo = false;
      this.conditionalAOB = false
      this.exportNoteReport = false;
      this.policeReport = false;
      this.progressNote = false;
      this.refferal = false;
      this.attorneyInfo = false;
      this.noFaultApplication = false;
      this.mrcctScanResults = false;
      this.affiInsurnace = false;
      this.superBill = false;
      this.policyDeclaration = false;
      this.acupConsent = false;
      this.nf2nf3Forms = false;
      this.aob = false;
      this.form = fb.group({
        id: [{ disabled: true }],
        practice: ['', Validators.required],
        doctor: ['', Validators.required],
        patientId: [''],
        firstName: ['', Validators.required],
        lastName: [''],
        initial: [''],
        address: [''],
        city: [''],
        state: ['', Validators.required],
        zipcode: ['', Validators.required],
        homePhone: [''],
        gender: ['', Validators.required],
        firstVisit: ['', Validators.required],
        dob: ['', Validators.required],
        caseType: ['', Validators.required],
        workPhone: [''],
        age: [''],
        patientInsFirstName: [''],
        patientInsLastName: [''],
        patientInsInitial: [''],
        patientInsAddress: [''],
        patientInsCity: [''],
        patientInsState: [''],
        patientInsZipcode: [''],
        patientInsPhone: [''],
        pInsCompany: ['', Validators.required],
        pClaim: [''],
        pCertifier: ['', Validators.required],
        pAdjuster: [''],
        pAdjusterfax: [''],
        doa: ['', Validators.required],
        pRelationship: [''],
        pNotes: [''],
        pPolicy: [''],
        sInsCompany: [''],
        sClaim: [''],
        sCertifier: [''],
        sAdjuster: [''],
        sAdjusterfax: [''],
        sRelationship: [''],
        sNotes: [''],
        sPolicy: [''],
        attorneyFirstName: [''],
        attorneyLastName: [''],
        attorneyPhone: [''],
        attorneyfax: [''],
        attorneyAddress: [''],
        attorneyCity: [''],
        attorneyState: [''],
        attorneyZipcode: [''],
        file: [''],
        batchDate: [''],
        dos: [''],
        demo: [false],
        insuranceInfo: [false],
        conditionalAOB: [false],
        exportNoteReport: [false],
        policeReport: [false],
        progressNote: [false],
        refferal: [false],
        attorneyInfo: [false],
        noFaultApplication: [false],
        mrcctScanResults: [false],
        affiInsurnace: [false],
        superBill: [false],
        policyDeclaration: [false],
        acupConsent: [false],
        nf2nf3Forms: [false],
        aob: [false],
      });
      this.practiceService.getAllPractice().subscribe(data => {
        this.newpractice = data;
        console.log(this.newpractice);
      });

      this.dataForEdit = this.activatedRoute.snapshot.paramMap.get("id");
      this.patientService.getAPatients(this.dataForEdit).subscribe(patientData => {//
        var gotPatientData = patientData;
        // var patientsGot = gotPatientData.filter(mapPatientData => {
        //   return this.dataForEdit == mapPatientData.id;
        // });
        console.log(gotPatientData);
        this.patientData = gotPatientData;
        var zipCodeData = '';
        if (!this.patientData.zipCode) {

        }
        else {
          this.patientData.zipCode = this.patientData.zipCode + '';

          for (var i = 0; i < this.patientData.zipCode.length; i++) {
            zipCodeData = zipCodeData + this.patientData.zipCode[i];
            if (i == 4) {
              zipCodeData = zipCodeData + '-';
            }
          }
          this.patientData.zipCode = zipCodeData;
        }
        var zipCodeDataAttorney = '';
        if (!this.patientData.attoneyZipCode) {

        }
        else {
          this.patientData.attoneyZipCode = this.patientData.attoneyZipCode + '';

          for (var i = 0; i < this.patientData.attoneyZipCode.length; i++) {
            zipCodeDataAttorney = zipCodeDataAttorney + this.patientData.attoneyZipCode[i];
            if (i == 4) {
              zipCodeDataAttorney = zipCodeDataAttorney + '-';
            }
          }
          this.patientData.attoneyZipCode = zipCodeDataAttorney;
        }
        var zipCodeDataPatient = '';
        if (!this.patientData.insZipCode) {

        }
        else {
          this.patientData.insZipCode = this.patientData.insZipCode + '';

          for (var i = 0; i < this.patientData.insZipCode.length; i++) {
            zipCodeDataPatient = zipCodeDataPatient + this.patientData.insZipCode[i];
            if (i == 4) {
              zipCodeDataPatient = zipCodeDataPatient + '-';
            }
          }
          this.patientData.insZipCode = zipCodeDataPatient;
        }
        var splitmissingInfo = this.patientData.missingInfo.split(",");
        this.form.get('practice').setValue(this.patientData.practiceCode);
        this.form.get('patientId').setValue(this.patientData.patientCode);
        this.form.get('firstName').setValue(this.patientData.firstName);
        this.form.get('lastName').setValue(this.patientData.lastName);
        this.form.get('initial').setValue(this.patientData.initial);
        this.form.get('address').setValue(this.patientData.address);
        this.form.get('city').setValue(this.patientData.city);
        this.form.get('state').setValue(this.patientData.state);
        this.form.get('zipcode').setValue(this.patientData.zipCode);
        this.form.get('homePhone').setValue(this.patientData.homePhone);
        this.form.get('gender').setValue(this.patientData.sex);
        this.form.get('firstVisit').setValue(this.patientData.firstVisit);
        this.form.get('dob').setValue(this.patientData.dob);
        this.form.get('caseType').setValue(this.patientData.caseType);
        this.form.get('workPhone').setValue(this.patientData.workPhone);
        this.form.get('age').setValue(this.patientData.age);
        this.form.get('patientInsFirstName').setValue(this.patientData.insFirstName);
        this.form.get('patientInsLastName').setValue(this.patientData.insLastName);
        this.form.get('patientInsInitial').setValue(this.patientData.insInitial);
        this.form.get('patientInsAddress').setValue(this.patientData.insAddress);
        this.form.get('patientInsCity').setValue(this.patientData.insCity);
        this.form.get('patientInsState').setValue(this.patientData.insState);
        this.form.get('patientInsZipcode').setValue(this.patientData.insZipCode);
        this.form.get('patientInsPhone').setValue(this.patientData.insPhoneNo);
        this.form.get('pInsCompany').setValue(this.patientData.primaryInsCo);
        this.form.get('pClaim').setValue(this.patientData.primaryInsClaimNo);
        this.form.get('pCertifier').setValue(this.patientData.primaryInsCertifier);
        this.form.get('pAdjuster').setValue(this.patientData.primaryInsAdjuster);
        this.form.get('pAdjusterfax').setValue(this.patientData.primaryInsAdjusterFax);
        this.form.get('doa').setValue(this.patientData.doa);
        this.form.get('pRelationship').setValue(this.patientData.primaryInsRelation);
        this.form.get('pNotes').setValue(this.patientData.primaryInsNotes);
        this.form.get('pPolicy').setValue(this.patientData.primaryInsPolicyNo);
        this.form.get('sInsCompany').setValue(this.patientData.secInsCo);
        this.form.get('sClaim').setValue(this.patientData.secInsClaimNo);
        this.form.get('sCertifier').setValue(this.patientData.secInsCertifier);
        this.form.get('sAdjuster').setValue(this.patientData.secInsAdjuster);
        this.form.get('sAdjusterfax').setValue(this.patientData.secInsAdjusterFax);
        this.form.get('sRelationship').setValue(this.patientData.secInsRelation);
        this.form.get('sNotes').setValue(this.patientData.secInsNotes);
        this.form.get('sPolicy').setValue(this.patientData.secInsPolicyNo);
        this.form.get('attorneyFirstName').setValue(this.patientData.attorneyFirstName);
        this.form.get('attorneyLastName').setValue(this.patientData.attorneyLastName);
        this.form.get('attorneyPhone').setValue(this.patientData.attorneyMobileNo);
        this.form.get('attorneyfax').setValue(this.patientData.attorneyFaxNo);
        this.form.get('attorneyAddress').setValue(this.patientData.attorneyAddress);
        this.form.get('attorneyCity').setValue(this.patientData.attorneyCity);
        this.form.get('attorneyState').setValue(this.patientData.attorneyState);
        this.form.get('attorneyZipcode').setValue(this.patientData.attoneyZipCode);


        //var splitArray = splitmissingInfo.split(",");
        for (var i = 0; i < splitmissingInfo.length; i++) {
          if (splitmissingInfo[i] == "Referral") {
            this.refferal = true;
            continue;
          }
          if (splitmissingInfo[i] == "Demo") {
            this.demo = true;
            continue;
          }
          if (splitmissingInfo[i] == "Super Bill") {
            this.superBill = true;
            continue;
          }
          if (splitmissingInfo[i] == "NF2/NF3 Forms") {
            this.nf2nf3Forms = true;
            continue;
          }
          if (splitmissingInfo[i] == "AOB") {
            this.aob = true;
            continue;
          }
          if (splitmissingInfo[i] == "Policy Declaration") {
            this.policyDeclaration = true;
            continue;
          }
          if (splitmissingInfo[i] == "Acupuncture Consent") {
            this.acupConsent = true;
            continue;
          }
          if (splitmissingInfo[i] == "Insurance Info") {
            this.insuranceInfo = true;
            continue;
          }
          if (splitmissingInfo[i] == "Conditional AOB") {
            this.conditionalAOB = true;
            continue;
          }
          if (splitmissingInfo[i] == "Affidavit of No Insurance") {
            this.affiInsurnace = true;
            continue;
          }
          if (splitmissingInfo[i] == "MRC/CT Scan Results") {
            this.mrcctScanResults = true;
            continue;
          }
          if (splitmissingInfo[i] == "Exam Note/ Report") {
            this.exportNoteReport = true;
            continue;
          }
          if (splitmissingInfo[i] == "Police Report") {
            this.policeReport = true;
            continue;
          }
          if (splitmissingInfo[i] == "Progress Notes") {
            this.progressNote = true;
            continue;
          }
          if (splitmissingInfo[i] == "Attorney Info") {
            this.attorneyInfo = true;
            continue;
          }
          if (splitmissingInfo[i] == "No-Fault Application") {
            this.noFaultApplication = true;
            continue;
          }
          if (splitmissingInfo[i].includes('dos')) {
            console.log('sdfjsfsd')
            var dosDataArray = splitmissingInfo[i].split(' ');
            console.log(dosDataArray)
            this.dos = dosDataArray[1];
            this.form.get('dos').setValue(dosDataArray[1]);
            continue;
          }
          
          if (splitmissingInfo[i].includes('batch')) {
            var dosDataArray = splitmissingInfo[i].split(' ');
            this.batchDate = dosDataArray[1];
            this.form.get('batchDate').setValue(dosDataArray[1]);
            continue;
          }
        }
      })


    } else {
      this.router.navigate(['']);
    }

  }

  zipcodeValidate(valueZip, eventVal) {
    console.log(eventVal);
    if (valueZip.length > 10) {
      this.form.get('zipcode').setValue(valueZip.substr(0, valueZip.length - 1));
    } else {
      if (valueZip.charCodeAt(valueZip.length - 1) >= 48 && valueZip.charCodeAt(valueZip.length - 1) <= 57) {
        if (valueZip.length === 5) {
          this.form.get('zipcode').setValue(valueZip + '-');
        }
      } else {
        this.form.get('zipcode').setValue(valueZip.substr(0, valueZip.length - 1));
      }
    }
  }

  attorneyzipCodeValidate(valueZip, eventVal) {
    console.log(eventVal);
    if (valueZip.length > 10) {
      this.form.get('attorneyZipcode').setValue(valueZip.substr(0, valueZip.length - 1));
    } else {
      if (valueZip.charCodeAt(valueZip.length - 1) >= 48 && valueZip.charCodeAt(valueZip.length - 1) <= 57) {
        if (valueZip.length === 5) {
          this.form.get('attorneyZipcode').setValue(valueZip + '-');
        }
      } else {
        this.form.get('attorneyZipcode').setValue(valueZip.substr(0, valueZip.length - 1));
      }
    }
  }

  patientInsZipcode(valueZip, eventVal) {
    console.log(eventVal);
    if (valueZip.length > 10) {
      this.form.get('patientInsZipcode').setValue(valueZip.substr(0, valueZip.length - 1));
    } else {
      if (valueZip.charCodeAt(valueZip.length - 1) >= 48 && valueZip.charCodeAt(valueZip.length - 1) <= 57) {
        if (valueZip.length === 5) {
          this.form.get('patientInsZipcode').setValue(valueZip + '-');
        }
      } else {
        this.form.get('patientInsZipcode').setValue(valueZip.substr(0, valueZip.length - 1));
      }
    }
  }


  onFileSelected() {
    const $pdf: any = document.querySelector('#file');
    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
      };
      reader.readAsArrayBuffer($pdf.files[0]);
    }
  }
  handleFileInput(files) {

    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   this.form.get('file').setValue(file);
    // }
    const formData = new FormData();
    this.filename = '/mapDocuments/' + files[0].name;
    formData.append('Document', files[0], files[0].name);
    //this.formD = formData;
    console.log(this.formD.get('Document'));
    this.onFileSelected();
  }
  ngOnInit() {
    // this.newpractice = JSON.parse(localStorage.getItem('practices'));
  }

  // change(value:any){
  // console.log('value is ',value)
  // }

  save() {
    console.log(this.form)
    if (!this.form.value.practice) {
      this.toastrManager.errorToastr('Practice Field is missing.', 'Missing Required Field.');
      this.form.get('practice').setValue('')
      return false;
    }
    // console.log(this.filename);
    // if (!this.filename) {
    //   this.toastrManager.errorToastr('Upload the file please', 'No File Uplaoded.');
    //   return false;
    // }
    // if (!this.form.value.patientId) {
    //   this.toastrManager.errorToastr('Patient ID Field is missing.', 'Missing Required Field.');
    //   this.form.get('patientId').setValue('')
    //   return false;
    // }

    if (!this.form.value.firstName) {
      this.toastrManager.errorToastr('First Name Field is missing.', 'Missing Required Field.');
      this.form.get('firstName').setValue('')
      return false;
    }
    // if (!this.form.value.lastName) {
    //   this.toastrManager.errorToastr('Last Name Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.initial) {
    //   this.toastrManager.errorToastr('Initial Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.address) {
    //   this.toastrManager.errorToastr('Address Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.city) {
    //   this.toastrManager.errorToastr('City Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    if (!this.form.value.state) {
      this.toastrManager.errorToastr('State Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.zipcode) {
      this.toastrManager.errorToastr('ZipCode Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.gender) {
      this.toastrManager.errorToastr('Gender Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.firstVisit) {
      this.toastrManager.errorToastr('First Visit Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.dob) {
      this.toastrManager.errorToastr('Date Od Birth Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.caseType) {
      this.toastrManager.errorToastr('Case Type Field is missing.', 'Missing Required Field.');
      return false;
    }
    // if (!this.form.value.age) {
    //   this.toastrManager.errorToastr('Age Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.homePhone) {
    //   this.toastrManager.errorToastr('Home Phone Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.workPhone) {
    //   this.toastrManager.errorToastr('Phone Number at Work Place Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsFirstName) {
    //   this.toastrManager.errorToastr('Patient Insurance First Name Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsLastName) {
    //   this.toastrManager.errorToastr('Patient Insurance Last Name Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsInitial) {
    //   this.toastrManager.errorToastr('Patient Insurance Initial Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsAddress) {
    //   this.toastrManager.errorToastr('Patient Insurance Address Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsCity) {
    //   this.toastrManager.errorToastr('Patient Insurance City Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsState) {
    //   this.toastrManager.errorToastr('Patient Insurance State Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsZipcode) {
    //   this.toastrManager.errorToastr('Patient Insurance ZipCode Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsPhone) {
    //   this.toastrManager.errorToastr('Patient Insurance Phone Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.patientInsPhone) {
    //   this.toastrManager.errorToastr('Patient Insurance Phone Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    if (!this.form.value.pInsCompany) {
      this.toastrManager.errorToastr('Patient Insurance Company Field is missing.', 'Missing Required Field.');
      return false;
    }
    // if (!this.form.value.pClaim) {
    //   this.toastrManager.errorToastr('Patient Insurance Claim Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    if (!this.form.value.doa) {
      this.toastrManager.errorToastr('Patient Insurance DOA Field is missing.', 'Missing Required Field.');
      return false;
    }
    if (!this.form.value.pCertifier) {
      this.toastrManager.errorToastr('Primary certifier Field is missing.', 'Missing Required Field.');
      return false;
    }
    // if (!this.form.value.pRelationship) {
    //   this.toastrManager.errorToastr('Patient Insurance Nominee Relationship Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.pNotes) {
    //   this.toastrManager.errorToastr('Patient Insurance Notes  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.pPolicy) {
    //   this.toastrManager.errorToastr('Patient Insurance Policy Number  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.sInsCompany) {
    //   this.toastrManager.errorToastr('Patient Insurance Secondary Company  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.sClaim) {
    //   this.toastrManager.errorToastr('Patient Insurance Secondary Claim  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.sRelationship) {
    //   this.toastrManager.errorToastr('Patient Insurance Secondary Nominee Relationship  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.sNotes) {
    //   this.toastrManager.errorToastr('Patient Insurance Secondary Notes  Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.sPolicy) {
    //   this.toastrManager.errorToastr('Patient Insurance Secondary Policy Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyFirstName) {
    //   this.toastrManager.errorToastr('Attorney FirstName Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyLastName) {
    //   this.toastrManager.errorToastr('Attorney LastName Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyPhone) {
    //   this.toastrManager.errorToastr('Attorney Phone Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyfax) {
    //   this.toastrManager.errorToastr('Attorney FAX Number Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyAddress) {
    //   this.toastrManager.errorToastr('Attorney Address Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyCity) {
    //   this.toastrManager.errorToastr('Attorney City Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyState) {
    //   this.toastrManager.errorToastr('Attorney State Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    // if (!this.form.value.attorneyZipcode) {
    //   this.toastrManager.errorToastr('Attorney ZIPCode Field is missing.', 'Missing Required Field.');
    //   return false;
    // }
    if (confirm('Are you sure you want to save the patient information?')) {
      //newpractice
      var practiceCode;
      console.log(this.form.value.practice);
      console.log(this.newpractice)
      for (var i = 0; i < this.newpractice.length; i++) {
        if (this.form.value.practice == this.newpractice[i].practiceCode) {
          practiceCode = this.newpractice[i].practiceCode
        }
      }
      console.log(practiceCode)
      console.log(this.form.value);
      //var dobDate = this.form.value.dob;
      var dobDateFormat = new Date(this.form.value.dob);
      var doaDateFormat = new Date(this.form.value.doa);
      var dobMonth = dobDateFormat.getMonth() + 1;
      var doaMonth = doaDateFormat.getMonth() + 1;
      var dosMonth = '';
      var dosDateFormat;
      console.log(this.form.value.dos)
      if (this.form.value.dos) {
        console.log('sdfhsgdkfjhskdfhjsdfk');
        dosDateFormat = new Date(this.form.value.dos)
        dosMonth = dosDateFormat.getMonth() + 1;
      }

      var batchDateMonth = '';
      var batchDateFormat;
      if (this.form.value.batchDate) {
        batchDateFormat = new Date(this.form.value.batchDate)
        batchDateMonth = batchDateFormat.getMonth() + 1;
      }
      var firstDateFormat = new Date(this.form.value.firstVisit);
      var firstVisitCreateDate = firstDateFormat.getMonth() + 1;
      var createdDate = new Date();
      var createPatient = createdDate.getMonth() + 1;
      var missingInfo = ''
      if (this.demo) {
        missingInfo = "Demo";
      }
      if (this.insuranceInfo) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Insurance Info";
        } else {
          missingInfo = "Insurance Info";
        }
      }
      if (this.conditionalAOB) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Conditional AOB";
        } else {
          missingInfo = "Conditional AOB";
        }
      }
      if (this.exportNoteReport) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Exam Note/ Report";
        } else {
          missingInfo = "Exam Note/ Report";
        }
      }
      if (this.policeReport) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Police Report";
        } else {
          missingInfo = "Police Report";
        }
      }
      if (this.progressNote) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Progress Notes";
        } else {
          missingInfo = "Progress Notes";
        }
      }
      console.log(this.refferal)
      if (this.refferal) {
        if (missingInfo) {
          console.log('dsfshdf')
          console.log(missingInfo)
          missingInfo = missingInfo + ',' + "Referral";
        } else {
          missingInfo = "Referral";
        }
      }
      if (this.attorneyInfo) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Attorney Info";
        } else {
          missingInfo = "Attorney Info";
        }
      }
      if (this.noFaultApplication) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "No-Fault Application";
        } else {
          missingInfo = "No-Fault Application";
        }
      }
      if (this.mrcctScanResults) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "MRC/CT Scan Results";
        } else {
          missingInfo = "MRC/CT Scan Results";
        }
      }
      if (this.affiInsurnace) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Affidavit of No Insurance";
        } else {
          missingInfo = "Affidavit of No Insurance";
        }
      }
      if (this.superBill) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Super Bill";
        } else {
          missingInfo = "Super Bill";
        }
      }
      if (this.policyDeclaration) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Policy Declaration";
        } else {
          missingInfo = "Policy Declaration";
        }
      }
      if (this.acupConsent) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "Acupuncture Consent";
        } else {
          missingInfo = "Acupuncture Consent";
        }
      }
      if (this.nf2nf3Forms) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "NF2/NF3 Forms";
        } else {
          missingInfo = "NF2/NF3 Forms";
        }
      }
      if (this.aob) {
        if (missingInfo) {
          missingInfo = missingInfo + ',' + "AOB";
        } else {
          missingInfo = "AOB";
        }
      }
      if (!dosMonth) { } else {
        if (missingInfo) {
          missingInfo = missingInfo + ',dos ' + dosDateFormat.getFullYear() + '-' + String(dosMonth).padStart(2, '0') + '-' + String(dosDateFormat.getDate()).padStart(2, '0');
        } else {
          missingInfo = "dos "+dosDateFormat.getFullYear() + '-' + String(dosMonth).padStart(2, '0') + '-' + String(dosDateFormat.getDate()).padStart(2, '0');
        }
      }
      if (!batchDateMonth) { } else {
        if (missingInfo) {
          missingInfo = missingInfo + ',batch ' + batchDateFormat.getFullYear() + '-' + String(batchDateMonth).padStart(2, '0') + '-' + String(batchDateFormat.getDate()).padStart(2, '0');
        } else {
          missingInfo = "batch "+batchDateFormat.getFullYear() + '-' + String(batchDateMonth).padStart(2, '0') + '-' + String(batchDateFormat.getDate()).padStart(2, '0');
        }
      }
      console.log(missingInfo);
      console.log(dosMonth);
      console.log(batchDateMonth)
      // let formData: FormData = new FormData();
      const send_form = {
        id:this.patientData.id,
        patientCode: this.form.value.patientId,
        firstName: this.form.value.firstName,
        lastName: this.form.value.lastName,
        sex: this.form.value.gender,
        address: this.form.value.address,
        city: this.form.value.city,
        state: this.form.value.state,
        zipCode: this.form.value.zipcode.replace(/-/g, ''),
        email: null,
        mobileNo: this.form.value.homePhone,
        enable: null,
        patientStatus: null,
        statusNotes: null,
        dob: dobDateFormat.getFullYear() + '-' + String(dobMonth).padStart(2, '0') + '-' + String(dobDateFormat.getDate()).padStart(2, '0'),
        caseType: this.form.value.caseType,
        workPhone: this.form.value.workPhone,
        age: this.form.value.age,
        initial :this.form.value.initial,
        homePhone:this.form.value.homePhone,
        insFirstName: this.form.value.patientInsFirstName,
        insLastName: this.form.value.patientInsLastName,
        insInitial: this.form.value.patientInsInitial,
        insAddress: this.form.value.patientInsAddress,
        insCity: this.form.value.patientInsCity,
        insState: this.form.value.patientInsState,
        insZipCode: this.form.value.patientInsZipcode.replace(/-/g, ''),
        insPhoneNo: this.form.value.patientInsPhone,
        primaryInsCo: this.form.value.pInsCompany,
        primaryInsClaimNo: this.form.value.pClaim,
        primaryInsPolicyNo: this.form.value.pPolicy,
        primaryInsRelation: this.form.value.pRelationship,
        primaryInsCertifier: this.form.value.pCertifier,
        primaryInsAdjuster: this.form.value.pAdjuster,
        primaryInsAdjusterFax: this.form.value.pAdjusterfax,
        primaryInsNotes: this.form.value.pNotes,
        doa: doaDateFormat.getFullYear() + '-' + String(doaMonth).padStart(2, '0') + '-' + String(doaDateFormat.getDate()).padStart(2, '0'),
        firstVisit: firstDateFormat.getFullYear() + '-' + String(firstVisitCreateDate).padStart(2, '0') + '-' + String(firstDateFormat.getDate()).padStart(2, '0'),
        secInsCo: this.form.value.sInsCompany,
        secInsClaimNo: this.form.value.sClaim,
        secInsPolicyNo: this.form.value.sPolicy,
        secInsRelation: this.form.value.sRelationship,
        secInsNotes: this.form.value.sNotes,
        secInsCertifier: this.form.value.sCertifier,
        secInsAdjuster: this.form.value.sAdjuster,
        secInsAdjusterFax: this.form.value.sAdjusterfax,
        addnInfo: null,
        attorneyFirstName: this.form.value.attorneyFirstName,
        attorneyLastName: this.form.value.attorneyLastName,
        attoneyAddress: this.form.value.attorneyAddress,
        attorneyCity: this.form.value.attorneyCity,
        attorneyState: this.form.value.attorneyState,
        attoneyZipCode: this.form.value.attorneyZipcode.replace(/-/g, ''),
        attorneyMobileNo: this.form.value.attorneyPhone,
        attorneyFaxNo: this.form.value.attorneyfax,
        practiceCode: practiceCode,
        createdBy: localStorage.getItem('USER_ID'),
        creationDate: createdDate.getFullYear() + '-' + String(createPatient).padStart(2, '0') + '-' + String(createdDate.getDate()).padStart(2, '0'),
        updatedBy: localStorage.getItem('USER_ID'),
        updatedDate: createdDate.getFullYear() + '-' + String(createPatient).padStart(2, '0') + '-' + String(createdDate.getDate()).padStart(2, '0'),
        attornyLogin: null,
        doctorId: null,
        doctorLogin: null,
        edocuments: [],
        missingInfo: missingInfo
      };
      //this.formD.append('FormValue','')
      //this.formD.append('FormValue', JSON.stringify(send_form));
      //this.formD.append('File', this.formD.get('Document'));
      // console.log(this.formD.get('File'));
      // console.log(this.formD.get('FormValue'));
      // console.log(this.formD);
      //var formValues = { "FormValue": send_form }
      console.log(send_form);
      //return;
      this.patientService.updatePatient(send_form).subscribe(data => {
        this.toastrManager.successToastr('Patient Updation', 'Patient Updated Successfully.');
        this.router.navigate(['mdmuser/patients']);
      }, error => {
        console.log(error);
        this.toastrManager.errorToastr(error.error.message, 'Patient Error.');
      });

      // this.patientService.create(this.form.value);
      // this.router.navigate(['/mdmuser/patients']);
      //   console.log(this.form.value);
      //   this.form.reset();
    }
    // this.patientService.create(this.form.value);
    // console.log(this.form.value);
    // this.form.reset();
  }

  workPhoneLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('workPhone').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  homePhoneLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('homePhone').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  patientInsPhoneLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('patientInsPhone').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  pAdjusterfaxLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('pAdjusterfax').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  sAdjusterfaxLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('sAdjusterfax').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  attorneyfaxLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('attorneyfax').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

  attorneyPhoneLimit(valueEvent) {
    if (valueEvent.length > 10) {
      this.form.get('attorneyPhone').setValue(valueEvent.substr(0, valueEvent.length - 1));
    }
  }

}
