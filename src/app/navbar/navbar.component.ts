import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  currentUser:any;
  currentDetails:any;
  isAdmin;
  isAttorney;
  isDoctor;
  isMdmUser;
  constructor(private route: ActivatedRoute,
    private router: Router) {
    this.currentUser = localStorage.getItem('USER_ROLE');
    console.log(localStorage.getItem("USER_NAME"));
    this.currentDetails = localStorage.getItem("USER_NAME");
    if (this.currentUser === 'admin') {
      this.isAdmin = true;
    }if(this.currentUser === 'attorney'){
      this.isAttorney=true;
    }
    if(this.currentUser === 'doctor'){
      this.isDoctor=true;
    }
    if(this.currentUser === 'user'){
      this.isMdmUser=true;
    }
   }

  ngOnInit() {
  }
  logout(){
    localStorage.removeItem("USER_NAME");
    localStorage.removeItem('USER_ROLE');
    localStorage.removeItem('USER_TOKEN');
    localStorage.removeItem('USER_ID');
    this.router.navigate(['']);
  }

}
