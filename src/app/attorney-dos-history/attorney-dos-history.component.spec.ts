import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyDosHistoryComponent } from './attorney-dos-history.component';

describe('AttorneyDosHistoryComponent', () => {
  let component: AttorneyDosHistoryComponent;
  let fixture: ComponentFixture<AttorneyDosHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyDosHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyDosHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
