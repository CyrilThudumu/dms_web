import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserPatientComponent } from './add-user-patient.component';

describe('AddUserPatientComponent', () => {
  let component: AddUserPatientComponent;
  let fixture: ComponentFixture<AddUserPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
